package com.support.qualifications.getqualifications;

/**
 * Created by joho on 2015/12/25.
 */
public final class Analyze_Calc {

    /**
     * 回答内容から正答率を算出し、結果を返す。
     * @param correct_info 回答内容(正解:1,不正解:0)を保持した配列
     * @return 正答率
     */
    public static final double correct_rate(int[] correct_info){

        double molec = 0;   //正解率計算の分子
        double denomin = 0; //正解率計算の分母

        int ans_count = correct_info.length; //回答数

        if( ans_count == 0 ){
            return 0;
        }

        for(int i = 0; i < ans_count ; i++){

            double infr = get_infuence_value(i,ans_count);

            molec += infr * correct_info[i];
            denomin += infr;
        }

        if( denomin == 0 ) {
            return 0;
        }
        else{
            return molec / denomin;
        }
    }

    /**
     * 回答内容から正答率を算出する際の分母・分子を計算し、結果を返す。
     * @param correct_info 回答内容(正解:1,不正解:0)を保持した配列
     * @return 正答率
     */
    public static final double[] molec_denomin(int[] correct_info){

        double[] molec_denomin = {0.0,1.0};//[0]:molec,[1]denomin

        int ans_count = correct_info.length; //回答数

        if( ans_count == 0 ){
            return molec_denomin;
        }

        for(int i = 0; i < ans_count ; i++){

            double infr = get_infuence_value(i,ans_count);

            molec_denomin[0] += infr * correct_info[i];
            molec_denomin[1] += infr;
        }

        /* 0除算防止 */
        if( molec_denomin[1] == 0 ) {
            return molec_denomin;
        }
        else{
            return molec_denomin;
        }
    }

    /**
     * 正答率計算に用いる、影響度を算出し、結果を返すメソッド
     * @param index 何番目の影響度か
     * @param ans_count いくつの回答を扱っているか
     * @return 影響度
     */
    private static final double get_infuence_value( int index , int ans_count){

        double infr;

        if( index == 0 ){
            infr = 1;
        }else{
            infr = get_infuence_value(index - 1, ans_count) - (1 / (double)ans_count);
        }

        return infr;
    }

    /**
     * 出題確率を計算するメソッド
     * @param r 順位
     * @param count_r 順位に該当する正答率の個数
     * @param t 順位rの総数
     * @return 出題確率
     */
    public static final double set_q_rate_infos( int r , int count_r , int t){

        double total = 0;

        for( int i = 1; i <= t; i ++ ){
            total += 1 / (double)i;
        }

        double p = ( 1 / ( r * total ) ) / (double)count_r;

        return p;
    }
}
