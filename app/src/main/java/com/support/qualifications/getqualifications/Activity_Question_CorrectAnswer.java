package com.support.qualifications.getqualifications;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

public class Activity_Question_CorrectAnswer extends Activity {

    private int mode_id;
    private int nextId;
    private String[] q_id_data;
    private String[] useranswers;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_correctanswer);

        /* xmlからViewを取得 */
        TextView correct_answerText = (TextView) findViewById(R.id.correct_answer);
        TextView user_answerText = (TextView) findViewById(R.id.user_answer);
        ImageView correct_mistakeImage = (ImageView) findViewById(R.id.correct_mistake_image);

        /* 前画面から問題IDリストと自分のIDの添え字を取得する */
        Intent intent = getIntent();
        mode_id = intent.getIntExtra(IntentKeyword.MODE_ID,0);
        int currentId = intent.getIntExtra(IntentKeyword.SELECT_Q_ID, 0);
        q_id_data = intent.getStringArrayExtra(IntentKeyword.Q_ID_LIST);

        String useranswer;

        if(mode_id == AiteaContract.MODE.TRIALEXAM) {
            useranswers = intent.getStringArrayExtra(IntentKeyword.SELECT_ANSWER_LIST);
            useranswer = useranswers[currentId];
        }else{
            useranswer = intent.getStringExtra(Activity_Question.SELECT_ANSWER);
        }



        /* 問題IDリストから、この画面を表示するのに必要な問題情報を得る */
        final question_info_data q_i_data = get_show_data(q_id_data[currentId]);

        String correctanswer = q_i_data.getCorrect_answer();

        correct_answerText.setText("正解　　　　：" + correctanswer);
        user_answerText.setText("あなたの回答：" + useranswer);

        int correct_mistake;

        if (useranswer.equals(correctanswer)) {
            correct_mistakeImage.setImageResource(R.drawable.icon_maru);
            correct_mistake = 1;
        } else {
            correct_mistakeImage.setImageResource(R.drawable.icon_batsu);
            correct_mistake = 0;
        }
        String nowDate = AiteaContract.getNowDate();

        /* 現在DBにあるidの最大値に+1して、最新のidとする */
        String next_answer_id = "A" + String.format("%05d", get_user_answer_id_count() + 1) ;
        String next_send_id = "S" +  String.format("%03d", get_send_id_count() + 1) ;

        /* 問題回答情報をDBにinsertする */
        add_answer_archive_info(next_answer_id, q_id_data[currentId], useranswer, correct_mistake, nowDate);
        add_server_send_info(next_send_id, next_answer_id, nowDate);

        /*  間違ったときに、DBの間違った回数をインクリメント */
        if( correct_mistake == 0 ){
            increment_mistake_count(q_id_data[currentId]);
        }

        final int AIM_ANS_COUNT = 10;

        int correct_info[] = get_correct_info(q_i_data.getField_code(),q_i_data.getL_class_code(),q_i_data.getM_class_code(),q_i_data.getS_class_code(),AIM_ANS_COUNT);

        double s_class_molec_denomin[] = Analyze_Calc.molec_denomin(correct_info);

        double s_class_correct_rate = s_class_molec_denomin[0] / s_class_molec_denomin[1];

        update_s_class_correct_info(q_i_data.getField_code(), q_i_data.getL_class_code(), q_i_data.getM_class_code(),q_i_data.getS_class_code(), s_class_molec_denomin, s_class_correct_rate);

        ////////////////////////
        // WebviewのURLを設定 //
        ////////////////////////

        // main.xmlのwebviewのIDを取得し、
        // WebViewActivityのWebViewクラスに関連付ける
        WebView webView = (WebView) findViewById(R.id.webview);
        webView.setWebViewClient(new set_position_WebViewClient());
        webView.loadUrl(geturl(q_i_data.getYear(), q_i_data.getTurn(), q_i_data.getQ_no()));


        ///////////////////
        // toolbarの表示 //
        ///////////////////

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_question_correctanswer);
        toolbar.setTitle("回答と解説");
        toolbar.setTitleTextColor(Color.WHITE);

        toolbar.inflateMenu(R.menu.question_correctanswer);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                boolean ret = true;
                switch (item.getItemId()) {

                    ////////////////////////////
                    // ボタン押下後、画面遷移 //
                    ////////////////////////////

                    // メニュー内の"右矢印"押下後の処理
                    case R.id.menu_nextquestiion:

                        if (nextId == q_id_data.length) {
                            finish();
                        } else {
                            if( mode_id == AiteaContract.MODE.TRIALEXAM ) {
                                //オブジェクト生成及びintentに遷移先のパッケージ名とアクティビティ名を引数に設定
                                Intent intent = new Intent(Activity_Question_CorrectAnswer.this, Activity_Question_CorrectAnswer.class);
                                //遷移先に渡す値
                                intent.putExtra(IntentKeyword.MODE_ID, mode_id);
                                intent.putExtra(IntentKeyword.Q_ID_LIST, q_id_data);
                                intent.putExtra(IntentKeyword.SELECT_Q_ID, nextId);
                                intent.putExtra(IntentKeyword.SELECT_ANSWER_LIST,useranswers);
                                //intentの実行
                                startActivity(intent);
                                finish();
                            }else{
                                //オブジェクト生成及びintentに遷移先のパッケージ名とアクティビティ名を引数に設定
                                Intent intent = new Intent(Activity_Question_CorrectAnswer.this, Activity_Question.class);
                                //遷移先に渡す値
                                intent.putExtra(IntentKeyword.MODE_ID, mode_id);
                                intent.putExtra(IntentKeyword.Q_ID_LIST, q_id_data);
                                intent.putExtra(IntentKeyword.SELECT_Q_ID, nextId);
                                //intentの実行
                                startActivity(intent);
                                finish();
                            }
                        }

                        ret = true;
                        break;
                }
                return ret;
            }
        });

        nextId = currentId + 1;
    }

    private void update_s_class_correct_info(int field_code,int l_class_code,int m_class_code,int s_class_code,double[] s_class_molec_denomin,double s_class_correct_rate){
        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        db.execSQL(DB_makesql.update_s_class_correct_rate(field_code, l_class_code, m_class_code, s_class_code, s_class_molec_denomin, s_class_correct_rate));

        db.close();
    }
    private int[] get_correct_info(int field_code, int l_class_code, int m_class_code, int s_class_code,int limit){

        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_correct_info(field_code, l_class_code, m_class_code, s_class_code, limit), null);

        int[] out_correct_info = new int[c.getCount()];

        int i = 0;
        while (c.moveToNext()) {
            /* データをテーブルから取得 */
            out_correct_info[i] = c.getInt(c.getColumnIndex(AiteaContract.ANSWER_ARCHIVE.CORRECT_MISTAKE));
            i++;
        }
        c.close();
        db.close();

        return out_correct_info;
    }

    private String geturl(String year, String turn, int q_no) {

        String ex_year;
        String ex_turn;

        if( turn.equals("春期") ){
            ex_turn = "haru";
        }else if( turn.equals("秋期") ){
            ex_turn = "aki";
        }
        else{
            return null;
        }

        /* 年度の数字部分を抜き取り */
        ex_year = year.substring(2,4);

        String url = "http://www." + AiteaContract.MY_EXAM_CATEGORY.toLowerCase() + "-siken.com/kakomon/" + ex_year + "_" + ex_turn + "/q" + q_no + ".html" + "#answerChar";

        return url;
    }

    private question_info_data get_show_data(String q_id_data){

        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        question_info_data out_q_i_data = new question_info_data();

        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_correctAnswer_info(q_id_data), null);

        /* sqlで実行した結果取得したテーブルを1行ずつ参照(今回は1行のみ) */
        while (c.moveToNext()) {
            /* データをテーブルから取得 */
            out_q_i_data.setYear(c.getString(c.getColumnIndex(AiteaContract.QUESTION_INFO.YEAR)));
            out_q_i_data.setTurn(c.getString(c.getColumnIndex(AiteaContract.QUESTION_INFO.TURN)));
            out_q_i_data.setQ_no(c.getInt(c.getColumnIndex(AiteaContract.QUESTION_INFO.Q_NO)));
            out_q_i_data.setField_code(c.getInt(c.getColumnIndex(AiteaContract.QUESTION_INFO.FIELD_CODE)));
            out_q_i_data.setL_class_code(c.getInt(c.getColumnIndex(AiteaContract.QUESTION_INFO.L_CLASS_CODE)));
            out_q_i_data.setM_class_code(c.getInt(c.getColumnIndex(AiteaContract.QUESTION_INFO.M_CLASS_CODE)));
            out_q_i_data.setS_class_code(c.getInt(c.getColumnIndex(AiteaContract.QUESTION_INFO.S_CLASS_CODE)));
            out_q_i_data.setCorrect_answer(c.getString(c.getColumnIndex(AiteaContract.QUESTION_INFO.CORRECT_ANSWER)));
        }

        c.close();
        db.close();

        return out_q_i_data;
    }


//        return correct_count;
//    }

    private int get_user_answer_id_count(){

        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        int answer_id_count = 0;

        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_user_answer_id_count(), null);

        /* sqlで実行した結果取得したテーブルを1行ずつ参照(今回は1行のみ) */
        if (c.moveToNext()) {
            /* データをテーブルから取得 */
            answer_id_count = c.getInt(c.getColumnIndex(AiteaContract.ANSWER_ARCHIVE_COUNT.ID));
        }

        c.close();
        db.close();

        return answer_id_count;
    }

    private int get_send_id_count(){

        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        int send_id_count = 0;

        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_send_id_count(), null);

        /* sqlで実行した結果取得したテーブルを1行ずつ参照(今回は1行のみ) */
        if (c.moveToNext()) {
            /* データをテーブルから取得 */
            send_id_count = c.getInt(c.getColumnIndex(AiteaContract.SERVER_SEND_INFO_COUNT.ID));
        }

        c.close();
        db.close();

        return send_id_count;
    }
    private void add_answer_archive_info(String user_answer_id,String q_id,String user_answer,int correct_mistake , String user_answer_date){
        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        db.execSQL(DB_makesql.add_answer_archive_info(user_answer_id, q_id, user_answer, correct_mistake, user_answer_date));

        db.close();
    }
    private void add_server_send_info(String send_id,String user_answer_id, String regist_date){
        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        db.execSQL(DB_makesql.add_server_send_info(send_id, user_answer_id, regist_date));

        db.close();
    }

    private void increment_mistake_count(String q_id){
        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        db.execSQL(DB_makesql.increment_mistake_count(q_id));

        db.close();
    }

}
