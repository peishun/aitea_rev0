// 参考URL http://ichitcltk.hustle.ne.jp/gudon2/index.php?pageType=file&id=Android028_ListView1

package com.support.qualifications.getqualifications;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.echo.holographlibrary.Bar;
import com.echo.holographlibrary.BarGraph;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class Activity_Question_Result extends Activity {

    private trial_exam_info_data[] trialExamInfoDatas;
    private ArrayList<User> users = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_result);

        ///////////////////
        // toolbarの表示 //
        ///////////////////

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_question_result);
        toolbar.setTitle("模擬試験結果");
        toolbar.setTitleTextColor(Color.WHITE);

        toolbar.inflateMenu(R.menu.question_result);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {

                    ////////////////////////////
                    // ボタン押下後、画面遷移 //
                    ////////////////////////////

                    // メニュー内の"ホーム"押下後の処理
                    case R.id.menu_home:
                        finish();
                        break;
                }
                return true;
            }
        });



        TextView passOrfallText = (TextView)findViewById(R.id.result_passOrfall);
        TextView correct_countText = (TextView)findViewById(R.id.result_correct_count);
        TextView correct_rateText = (TextView)findViewById(R.id.result_correct_rate);
        TextView elapsed_timeText = (TextView)findViewById(R.id.result_elapsed_time);

        //リストに表示するデータの配列

        trialExamInfoDatas = get_trial_exam_info();
        delete_tbl_info(AiteaContract.TRIAL_EXAM_INFO.TABLE_NAME);

        String[] q_names = get_q_names(trialExamInfoDatas);
        add_answer_archive_and_send_info(trialExamInfoDatas);
        increment_mistake_count(trialExamInfoDatas);

        int i = 0;
        int Answer_questionNum = 0;
        int correct_count = 0;
        int sumElapsedtime = 0;



        while (i < trialExamInfoDatas.length ) {

            if( trialExamInfoDatas[i].getUser_answer() != null ) {
                User user = new User();
                if (trialExamInfoDatas[i].getCorrect_mistake() == 1) {
                    user.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.icon_maru));
                    correct_count++;
                } else {
                    user.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.icon_batsu));
                }
                sumElapsedtime += trialExamInfoDatas[i].getElapsed_time();
                user.setQ_info(i + 1);
                user.setQ_summary(q_names[i]);
                users.add(user);
                Answer_questionNum++;
            }
            i++;
        }

        String passOrfall;

        if( (float)correct_count / trialExamInfoDatas.length >= 0.6 ){
            passOrfall = "「合格」";
        }else{
            passOrfall = "「不合格」";
        }

        passOrfallText.setText("あなたは" + passOrfall + "です");

        float correct_rate;
        if(Answer_questionNum == 0){
            correct_rate = (float)correct_count / 1 * 100;
        }else {
            correct_rate = (float) correct_count / Answer_questionNum * 100;
        }

        correct_rateText.setText( "正解率：" + String.format("%.1f",correct_rate) + "%" );
        elapsed_timeText.setText((sumElapsedtime / 60 ) + "分" );

        String next_performance_id = "P" +  String.format("%03d", get_performance_id_count() + 1);
        String nowDate = AiteaContract.getNowDate();
        add_performance_info(next_performance_id, nowDate, correct_count);

        performance_info_data[] performanceInfoData = get_performance();
//        int peform_diff = performanceInfoData[0].getPerformance() - performanceInfoData[1].getPerformance();
//        String sign;
//        if(peform_diff > 0){
//            sign = "+";
//        }else if(peform_diff < 0){
//            sign = "-";
//        }else{
//            sign = "±";
//        }
//        correct_countText.setText("　成績：" + correct_count + "/" + Answer_questionNum + "（" + "前回比：" + sign + peform_diff + "）" );

        correct_countText.setText("　成績：" + correct_count + "/" + Answer_questionNum);
    }

    @Override
    protected void onResume() {
        super.onResume();

        //リストビューの取得
        ListView myListview = (ListView)findViewById(R.id.listview_question_result_list);

        //リストに表示するデータをアダプターにセット
        UserAdapter adapter = new UserAdapter(this, 0, users);

        myListview.setAdapter(adapter);

        //リストをタップしたときのEvent
        myListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(
                    AdapterView<?> parent,
                    View view, //タップされたビュー
                    int position, //何番目？
                    long id //View id
            ) {

                // オブジェクト生成及びintentに遷移先のパッケージ名とアクティビティ名を引数に設定
                Intent intent = new Intent(Activity_Question_Result.this, Activity_Question_CorrectAnswer.class);

                intent.putExtra(IntentKeyword.MODE_ID, AiteaContract.MODE.TRIALEXAM);
                intent.putExtra(IntentKeyword.SELECT_Q_ID, position);
                intent.putExtra(IntentKeyword.Q_ID_LIST, trial_exam_info_data_convert_q_id(trialExamInfoDatas));
                intent.putExtra(IntentKeyword.SELECT_ANSWER_LIST, trial_exam_info_data_convert_select_answer(trialExamInfoDatas));

                // intentの実行
                startActivity(intent);

            }
        });
        final ScrollView scrollView = (ScrollView)findViewById(R.id.result_scrollview);
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(scrollView.FOCUS_UP);
            }
        });
    }

    private String[] get_q_names(trial_exam_info_data[] q_id_data){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        String[] out_q_name = new String[q_id_data.length];

        for( int i = 0; i < q_id_data.length; i++ ){
            Cursor c = null;
            c = db.rawQuery(DB_makesql.get_q_name(q_id_data[i].getQ_id()), null);
            if (c.moveToNext()) {
                out_q_name[i] = new String();
                out_q_name[i] = c.getString(c.getColumnIndex(AiteaContract.QUESTION_INFO.Q_NAME));
            }
            c.close();
        }
        db.close();

        return out_q_name;
    }

    private String[] trial_exam_info_data_convert_q_id(trial_exam_info_data[] trialExamInfoDatas){

        ArrayList<String> q_id_list = new ArrayList<>();

        for( int i = 0; i < trialExamInfoDatas.length; i++ ){

            if( trialExamInfoDatas[i].getUser_answer() != null ) {
                q_id_list.add(trialExamInfoDatas[i].getQ_id());
            }
        }

        String[] q_id_data = (String[])q_id_list.toArray(new String[q_id_list.size()]);

        return q_id_data;

    }
    private String[] trial_exam_info_data_convert_select_answer(trial_exam_info_data[] trialExamInfoDatas) {

        ArrayList<String> select_answer_list = new ArrayList<>();

        for (int i = 0; i < trialExamInfoDatas.length; i++) {

            if (trialExamInfoDatas[i].getUser_answer() != null) {
                select_answer_list.add(trialExamInfoDatas[i].getUser_answer());
            }
        }

        String[] select_answer_data = (String[]) select_answer_list.toArray(new String[select_answer_list.size()]);

        return select_answer_data;
    }


    private trial_exam_info_data[] get_trial_exam_info(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_trial_exam_info(), null);

        trial_exam_info_data[] trialExamInfoDatas = new trial_exam_info_data[c.getCount()];

        int i = 0;
        while (c.moveToNext()) {
            trialExamInfoDatas[i] = new trial_exam_info_data();
            trialExamInfoDatas[i].setQ_id(c.getString(c.getColumnIndex(AiteaContract.TRIAL_EXAM_INFO.Q_ID)));
            trialExamInfoDatas[i].setUser_answer(c.getString(c.getColumnIndex(AiteaContract.TRIAL_EXAM_INFO.USER_ANSWER)));
            trialExamInfoDatas[i].setCorrect_mistake(c.getInt(c.getColumnIndex(AiteaContract.TRIAL_EXAM_INFO.CORRECT_MISTAKE)));
            trialExamInfoDatas[i].setUser_answer_date(c.getString(c.getColumnIndex(AiteaContract.TRIAL_EXAM_INFO.USER_ANSWER_DATE)));
            trialExamInfoDatas[i].setElapsed_time(c.getInt(c.getColumnIndex(AiteaContract.TRIAL_EXAM_INFO.ELAPSED_TIME)));
            i++;
        }

        c.close();
        db.close();

        return trialExamInfoDatas;
    }

    private performance_info_data[] get_performance(){

        final int GET_PERFORMANCE_LIMIT = 2;

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_performance(GET_PERFORMANCE_LIMIT), null);

        performance_info_data[] performances = new performance_info_data[c.getCount()];

        int i = 0;
        while (c.moveToNext()) {
            performances[i] = new performance_info_data();
            performances[i].setPerformance(c.getInt(c.getColumnIndex(AiteaContract.PERFORMANCE_INFO.PERFORMANCE)));;
            performances[i].setUser_answer_date(c.getString(c.getColumnIndex(AiteaContract.PERFORMANCE_INFO.USER_ANSWER_DATE)));
            i++;
        }

        c.close();
        db.close();

        return performances;
    }

    public class UserAdapter extends ArrayAdapter<User> {

        private LayoutInflater layoutInflater;

        public UserAdapter(Context c, int id, ArrayList<User> users) {
            super(c, id, users);
            this.layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        /* リストをスクロールして、新たにリストが表示されるときに呼ばれる */
        @Override
        public View getView(int pos, View convertview, ViewGroup parent){

            ViewHolder holder;

            /* 再利用されるビューが無い場合、 */
            if( convertview == null ){
                convertview = layoutInflater.inflate(
                        R.layout.question_list_item,
                        parent,
                        false
                );
                holder = new ViewHolder();
                holder.icon = (ImageView) convertview.findViewById(R.id.questionicon);
                holder.info = (TextView) convertview.findViewById(R.id.questionInfo);
                holder.summary = (TextView) convertview.findViewById(R.id.questionsummary);
                convertview.setTag(holder);//データ保持
            }else{
                holder = (ViewHolder)convertview.getTag();
            }

            /* Userデータを取ってきて表示 */
            User user = (User)getItem(pos);

            holder.icon.setImageBitmap(user.getIcon());
            holder.info.setText("問" + user.getQ_info());
            holder.summary.setText(user.getQ_summary());

            return convertview;

        }
    }
    private void delete_tbl_info(String table_name){

        /* DatabaseとOpenHelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

            /* sql文実行 */
        db.delete(table_name, null, null);
        db.close();
    }

    //リストに表示するデータ定義
    static class ViewHolder{
        ImageView icon;
        TextView info;
        TextView summary;
    }

    //リストに表示するデータ型クラス
    public class User{
        private Bitmap icon;
        private int q_info;
        private String q_summary;

        public void setIcon(Bitmap icon) {
            this.icon = icon;
        }

        public void setQ_info(int q_info) {
            this.q_info = q_info;
        }

        public void setQ_summary(String q_summary) {
            this.q_summary = q_summary;
        }

        public Bitmap getIcon() {

            return icon;
        }

        public int getQ_info() {
            return q_info;
        }

        public String getQ_summary() {
            return q_summary;
        }
    }

    private void add_performance_info(String peformance_id,String user_answer_date,int peformance){
        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        db.execSQL(DB_makesql.add_performance_info(peformance_id, user_answer_date, peformance));

        db.close();
    }
    private int get_performance_id_count(){

        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        int performance_id_count = 0;

        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_performance_id_count(), null);

        /* sqlで実行した結果取得したテーブルを1行ずつ参照(今回は1行のみ) */
        if (c.moveToNext()) {
            /* データをテーブルから取得 */
            performance_id_count = c.getInt(c.getColumnIndex(AiteaContract.PERFORMANCE_INFO_COUNT.ID));
        }

        c.close();
        db.close();

        return performance_id_count;
    }
    private int get_user_answer_id_count(){

        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        int answer_id_count = 0;

        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_user_answer_id_count(), null);

        /* sqlで実行した結果取得したテーブルを1行ずつ参照(今回は1行のみ) */
        if (c.moveToNext()) {
            /* データをテーブルから取得 */
            answer_id_count = c.getInt(c.getColumnIndex(AiteaContract.ANSWER_ARCHIVE_COUNT.ID));
        }

        c.close();
        db.close();

        return answer_id_count;
    }

    private int get_send_id_count(){

        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        int send_id_count = 0;

        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_send_id_count(), null);

        /* sqlで実行した結果取得したテーブルを1行ずつ参照(今回は1行のみ) */
        if (c.moveToNext()) {
            /* データをテーブルから取得 */
            send_id_count = c.getInt(c.getColumnIndex(AiteaContract.SERVER_SEND_INFO_COUNT.ID));
        }

        c.close();
        db.close();

        return send_id_count;
    }

    private void add_answer_archive_and_send_info(trial_exam_info_data[] trialExamInfoDatas){
        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        int user_answer_id_count = get_user_answer_id_count() + 1;
        int send_id_count = get_send_id_count() + 1;

        int i = 0;
        int j = 0;
        while (i < trialExamInfoDatas.length ) {

            if( trialExamInfoDatas[i].getUser_answer() != null ) {
                String new_answer_id = "A" + String.format("%05d", user_answer_id_count + j) ;
                String new_send_id = "S" +  String.format("%03d", send_id_count + j) ;

                db.execSQL(DB_makesql.add_answer_archive_info(new_answer_id, trialExamInfoDatas[i].getQ_id(), trialExamInfoDatas[i].getUser_answer(), trialExamInfoDatas[i].getCorrect_mistake(), trialExamInfoDatas[i].getUser_answer_date()));
                db.execSQL(DB_makesql.add_server_send_info(new_send_id, new_answer_id, trialExamInfoDatas[i].getUser_answer_date()));
                j++;
            }
            i++;
        }
        db.close();
    }

    private void increment_mistake_count(trial_exam_info_data[] trialExamInfoDatas){
        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        for(int i = 0; i < trialExamInfoDatas.length; i++) {

            if (trialExamInfoDatas[i].getCorrect_mistake() == 0) {
                db.execSQL(DB_makesql.increment_mistake_count(trialExamInfoDatas[i].getQ_id()));
            }

        }
        db.close();
    }

    /////グラフ出す場合に使用する
//
//    private void createBarChart() {
//
//        BarGraph barGraph = (BarGraph)findViewById(R.id.result_bar_chart);
//        barGraph.setBars(createBarChartData());
//
//    }
//    private ArrayList<Bar> createBarChartData() {
//
//        ArrayList<Bar> BarChartDatas = new ArrayList<Bar>();
//
//
//        for( int i = performanceInfoData.length - 1; i >=0 ; i--){
//            Bar b = new Bar();
//            b.setColor(R.color.red);
//            b.setName(getMonthAndDay(performanceInfoData[i].getUser_answer_date()));
//            b.setValue(performanceInfoData[i].getPerformance());
//            BarChartDatas.add(b);
//        }
//
//        return BarChartDatas;
//    }
//
//    private String getMonthAndDay(String Date){
//        return Integer.valueOf(Date.substring(5,7)) + "/"  + Integer.valueOf(Date.substring(8, 10));
//    }

}
