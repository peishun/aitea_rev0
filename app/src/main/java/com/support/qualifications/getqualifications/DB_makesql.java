package com.support.qualifications.getqualifications;


import android.graphics.Bitmap;

import java.util.HashMap;

/**
 *　SQL文を作成するメソッドをまとめたクラス
 */
public final class DB_makesql {

    public DB_makesql() {}

    /**
     * 引数で指定した問題IDに該当する問題回答画面を表示するのに必要なデータを取得するsql文を返す
     * @param q_id 問題ID
     * @return 作成したsql文
     */
    public  static final String get_q_info(String q_id) {

        String sql = "SELECT " +
                AiteaContract.QUESTION_INFO.YEAR + "," +
                AiteaContract.QUESTION_INFO.TURN + "," +
                AiteaContract.QUESTION_INFO.Q_NO + "," +
                AiteaContract.QUESTION_INFO.Q_NAME + "," +
                AiteaContract.QUESTION_INFO.Q_SENTENCE + "," +
                AiteaContract.QUESTION_INFO.Q_IMAGE + "," +
                AiteaContract.QUESTION_INFO.SELECTION_SENTENCE_A + "," +
                AiteaContract.QUESTION_INFO.SELECTION_SENTENCE_I + "," +
                AiteaContract.QUESTION_INFO.SELECTION_SENTENCE_U + "," +
                AiteaContract.QUESTION_INFO.SELECTION_SENTENCE_E + "," +
                AiteaContract.QUESTION_INFO.SELECTION_IMAGE + "," +
                AiteaContract.QUESTION_INFO.Q_CORRECT_RATE + "," +
                AiteaContract.QUESTION_INFO.FAVORITE_FLAG + "," +
                AiteaContract.QUESTION_INFO.OVERCOME_FLAG +
                " FROM " + AiteaContract.QUESTION_INFO.TABLE_NAME +
                " WHERE " + AiteaContract.QUESTION_INFO.Q_ID + "=" + "\'" + q_id + "\'";

        return sql;
    }

    /**
     * 引数で指定した問題IDに該当する問題リストを表示するのに必要なデータを取得するsql文を返す
     * @param q_id 問題ID
     * @return 作成したsql文
     */
    public  static final String get_qlst_info(String q_id) {

        String sql = "SELECT " +
                AiteaContract.QUESTION_INFO.YEAR + "," +
                AiteaContract.QUESTION_INFO.TURN + "," +
                AiteaContract.QUESTION_INFO.Q_NO + "," +
                AiteaContract.QUESTION_INFO.Q_NAME +
                " FROM " + AiteaContract.QUESTION_INFO.TABLE_NAME +
                " WHERE " + AiteaContract.QUESTION_INFO.Q_ID + "=" + "\'" + q_id + "\'";

        return sql;
    }
    /**
     * 全ての分野コード、分野名を取得するsql文を返す
     * @return 作成したsql文
     */
    public static final String get_all_field_info(){
        String sql = "SELECT " + "DISTINCT " +
                AiteaContract.GENRE_INFO.FIELD_CODE + "," +
                AiteaContract.GENRE_INFO.FIELD_NAME +
                " FROM " + AiteaContract.GENRE_INFO.TABLE_NAME;
        return sql;
    }

    /**
     * 引数で指定した分野コードに属する大分類コード、大分類名を取得するsql文を返す
     * @param field_code 分野コード
     * @return 作成したsql文
     */
    public static final String get_l_class_info(int field_code){
        String sql = "SELECT " + "DISTINCT " +
                AiteaContract.GENRE_INFO.L_CLASS_CODE + "," +
                AiteaContract.GENRE_INFO.L_CLASS_NAME +
                " FROM " + AiteaContract.GENRE_INFO.TABLE_NAME +
                " WHERE " + AiteaContract.GENRE_INFO.FIELD_CODE + "=" + field_code;
        return sql;
    }

    /**
     * 引数で指定した分野コードと大分類コードに属する中分類コード、中分類名を取得するsql文を返す
     * @param field_code 分野コード
     * @param l_class_code 大分類コード
     * @return 作成したsql文
     */
    public static final String get_m_class_info(int field_code,int l_class_code){
        String sql = "SELECT " + "DISTINCT " +
                AiteaContract.GENRE_INFO.M_CLASS_CODE + "," +
                AiteaContract.GENRE_INFO.M_CLASS_NAME +
                " FROM " + AiteaContract.GENRE_INFO.TABLE_NAME +
                " WHERE " + AiteaContract.GENRE_INFO.FIELD_CODE + "=" + field_code +
                " AND " +  AiteaContract.GENRE_INFO.L_CLASS_CODE + "=" + l_class_code;
        return sql;
    }

    /**
     * 引数で指定した分野コードと大分類コードと中分類コードに属する小分類コード、小分類名を取得するsql文を返す
     * @param field_code 分野コード
     * @param l_class_code 大分類コード
     * @param m_class_code 中分類コード
     * @return 作成したsql文
     */
    public static final String get_s_class_info(int field_code,int l_class_code,int m_class_code){
        String sql = "SELECT " + "DISTINCT " +
                AiteaContract.GENRE_INFO.S_CLASS_CODE + "," +
                AiteaContract.GENRE_INFO.S_CLASS_NAME +
                " FROM " + AiteaContract.GENRE_INFO.TABLE_NAME +
                " WHERE " + AiteaContract.GENRE_INFO.FIELD_CODE + "=" + field_code +
                " AND " + AiteaContract.GENRE_INFO.L_CLASS_CODE + "=" + l_class_code +
                " AND " + AiteaContract.GENRE_INFO.M_CLASS_CODE + "=" + m_class_code;
        return sql;
    }

    /**
     * 引数で指定した分野コードに属する問題IDを年度、回次で降順、問題IDで昇順で取得するsql文を返す
     * @param field_code 分野コード
     * @return 作成したsql文
     */
    public final static String get_q_id(int field_code){
        String sql = "SELECT " +
                AiteaContract.QUESTION_INFO.Q_ID +
                " FROM " + AiteaContract.QUESTION_INFO.TABLE_NAME +
                " WHERE " + AiteaContract.QUESTION_INFO.FIELD_CODE + "=" +  field_code +
                " ORDER BY " + AiteaContract.QUESTION_INFO.YEAR + " DESC" + "," +
                AiteaContract.QUESTION_INFO.TURN + " DESC" +"," + AiteaContract.QUESTION_INFO.Q_NO;
        return sql;
    }
    /**
     * 引数で指定した分野コードと大分類コードに属する問題IDを年度、回次で降順、問題IDで昇順で取得するsql文を返す
     * @param field_code 分野コード
     * @param l_class_code 大分類コード
     * @return 作成したsql文
     */
    public final static String get_q_id(int field_code , int l_class_code){
        String sql = "SELECT " +
                AiteaContract.QUESTION_INFO.Q_ID +
                " FROM " + AiteaContract.QUESTION_INFO.TABLE_NAME +
                " WHERE " + AiteaContract.QUESTION_INFO.FIELD_CODE + "=" +  field_code +
                " AND " + AiteaContract.QUESTION_INFO.L_CLASS_CODE + "=" +  l_class_code +
                " ORDER BY " + AiteaContract.QUESTION_INFO.YEAR + " DESC" + "," +
                AiteaContract.QUESTION_INFO.TURN + " DESC" +"," + AiteaContract.QUESTION_INFO.Q_NO;
        return sql;
    }

    /**
     * 引数で指定した分野コードと大分類コードと中分類コードに属する問題IDを年度、回次で降順、問題IDで昇順で取得するsql文を返す
     * @param field_code 分野コード
     * @param l_class_code 大分類コード
     * @param m_class_code 中分類コード
     * @return 作成したsql文
     */
    public final static String get_q_id(int field_code , int l_class_code , int m_class_code){
        String sql = "SELECT " +
                AiteaContract.QUESTION_INFO.Q_ID +
                " FROM " + AiteaContract.QUESTION_INFO.TABLE_NAME +
                " WHERE " + AiteaContract.QUESTION_INFO.FIELD_CODE + "=" +  field_code +
                " AND " + AiteaContract.QUESTION_INFO.L_CLASS_CODE + "=" +  l_class_code +
                " AND " + AiteaContract.QUESTION_INFO.M_CLASS_CODE + "=" +  m_class_code +
                " ORDER BY " + AiteaContract.QUESTION_INFO.YEAR + " DESC" + "," +
                AiteaContract.QUESTION_INFO.TURN + " DESC" +"," + AiteaContract.QUESTION_INFO.Q_NO;
        return sql;
    }

    /**
     * 引数で指定した分野コードと大分類コードと中分類コードと小分類コードに属する問題IDを年度、回次で降順、問題IDで昇順で取得するsql文を返す
     * @param field_code 分野コード
     * @param l_class_code 大分類コード
     * @param m_class_code 中分類コード
     * @param s_class_code 小分類コード
     * @return 作成したsql文
     */
    public final static String get_q_id(int field_code , int l_class_code , int m_class_code , int s_class_code){
        String sql = "SELECT " +
                AiteaContract.QUESTION_INFO.Q_ID +
                " FROM " + AiteaContract.QUESTION_INFO.TABLE_NAME +
                " WHERE " + AiteaContract.QUESTION_INFO.FIELD_CODE + "=" +  field_code +
                " AND " + AiteaContract.QUESTION_INFO.L_CLASS_CODE + "=" +  l_class_code +
                " AND " + AiteaContract.QUESTION_INFO.M_CLASS_CODE + "=" +  m_class_code +
                " AND " + AiteaContract.QUESTION_INFO.S_CLASS_CODE + "=" +  s_class_code +
                " ORDER BY " + AiteaContract.QUESTION_INFO.YEAR + " DESC" + "," +
                AiteaContract.QUESTION_INFO.TURN + " DESC" +"," + AiteaContract.QUESTION_INFO.Q_NO;
        return sql;
    }
    /**
     * 引数で指定した年度、回次に属する問題IDを問題IDで昇順で取得するsql文を返す
     * @param year 年度
     * @param turn　回次
     * @return 作成したsql文
     */
    public final static String get_q_id(String year , String turn){
        String sql = "SELECT " +
                AiteaContract.QUESTION_INFO.Q_ID +
                " FROM " + AiteaContract.QUESTION_INFO.TABLE_NAME +
                " WHERE " + AiteaContract.QUESTION_INFO.YEAR + "=" + "\'" + year + "\'"+
                " AND " + AiteaContract.QUESTION_INFO.TURN + "=" + "\'" + turn + "\'"+
                " ORDER BY " + AiteaContract.QUESTION_INFO.Q_NO;
        return sql;
    }

    /**
     * 全ての年度、回次を降順で取得するsql文を返す
     * @return 作成したsql文
     */
    public final static String get_all_year_turn_info(){
        String sql = "SELECT " + "DISTINCT " +
                AiteaContract.QUESTION_INFO.YEAR + "," +
                AiteaContract.QUESTION_INFO.TURN +
                " FROM " + AiteaContract.QUESTION_INFO.TABLE_NAME +
                " ORDER BY " + AiteaContract.QUESTION_INFO.YEAR + " DESC" + "," +
                AiteaContract.QUESTION_INFO.TURN + " DESC";
        return sql;
    }

    /**
     * 引数で指定した問題IDに該当する、解答画面表示に必要な情報を表示する
     * @param q_id
     * @return
     */
    public final static String get_correctAnswer_info(String q_id){
        String sql = "SELECT " +
                AiteaContract.QUESTION_INFO.YEAR + "," +
                AiteaContract.QUESTION_INFO.TURN + "," +
                AiteaContract.QUESTION_INFO.Q_NO + "," +
                AiteaContract.QUESTION_INFO.FIELD_CODE + "," +
                AiteaContract.QUESTION_INFO.L_CLASS_CODE + "," +
                AiteaContract.QUESTION_INFO.M_CLASS_CODE + "," +
                AiteaContract.QUESTION_INFO.S_CLASS_CODE + "," +
                AiteaContract.QUESTION_INFO.CORRECT_ANSWER +
                " FROM " +  AiteaContract.QUESTION_INFO.TABLE_NAME +
                " WHERE " + AiteaContract.QUESTION_INFO.Q_ID + "=" + "\'" + q_id + "\'";
        return sql;
    }

    /**
     SELECT correct_mistake
     FROM tbl_answer_archive as t1 INNER JOIN tbl_question_info as t2 ON t1.question_id = t2.question_id
     WHERE t2.field_code=1 and t2.large_class_code=2 and t2.middle_class_code= 1 and t2.small_class_code=1 order by user_answer_id limit 10
     */
    public static final String get_correct_info(int field_code, int l_class_code, int m_class_code, int s_class_code,int limit){
        String sql = "SELECT " + AiteaContract.ANSWER_ARCHIVE.CORRECT_MISTAKE +
                " FROM " + AiteaContract.ANSWER_ARCHIVE.TABLE_NAME  + " as t1" +
                " INNER JOIN " + AiteaContract.QUESTION_INFO.TABLE_NAME + " as t2" +
                " ON " + "t1." + AiteaContract.ANSWER_ARCHIVE.Q_ID + "=" + "t2." + AiteaContract.QUESTION_INFO.Q_ID  +
                " WHERE " + "t2." + AiteaContract.QUESTION_INFO.FIELD_CODE + "=" + field_code +
                " AND " + "t2." + AiteaContract.QUESTION_INFO.L_CLASS_CODE + "=" + l_class_code +
                " AND " + "t2." + AiteaContract.QUESTION_INFO.M_CLASS_CODE + "=" + m_class_code +
                " AND " + "t2." + AiteaContract.QUESTION_INFO.S_CLASS_CODE + "=" + s_class_code +
                " ORDER BY " + AiteaContract.ANSWER_ARCHIVE.USER_ANSWER_ID + " DESC "+ "LIMIT " + limit;
        return sql;
    }

    /**
     * サーバ送信情報テーブルの全てのデータを取得するsql文を返す
     * @return 作成したsql文
     */
    public static final String get_server_send_info() {
        String sql = "SELECT " +
                "\'" + AiteaContract.MY_EXAM_CATEGORY + "\'" + " as exam_category," +
                AiteaContract.ANSWER_ARCHIVE.Q_ID + "," +
                AiteaContract.ANSWER_ARCHIVE.USER_ANSWER + "," +
                AiteaContract.ANSWER_ARCHIVE.CORRECT_MISTAKE +
                " FROM " + AiteaContract.SERVER_SEND_INFO.TABLE_NAME + " as t1" +
                " INNER JOIN " + AiteaContract.ANSWER_ARCHIVE.TABLE_NAME + " as t2" +
                " ON t1." + AiteaContract.SERVER_SEND_INFO.USER_ANSWER_ID + " = t2." + AiteaContract.ANSWER_ARCHIVE.USER_ANSWER_ID +
                " ORDER BY " + AiteaContract.SERVER_SEND_INFO.SEND_ID + " ASC";

        return sql;
    }


    public static final String add_answer_archive_info(String user_answer_id,String q_id,String user_answer,int correct_mistake , String user_answer_date){
        String sql = "INSERT INTO " + AiteaContract.ANSWER_ARCHIVE.TABLE_NAME +
                " VALUES " + "(" +
                "\'" + user_answer_id + "\'" + "," +
                "\'" + q_id + "\'" + "," +
                "\'" + user_answer + "\'" + "," +
                correct_mistake + "," +
                "\'" + user_answer_date + "\'" + " )";
        return sql;
    }

    public static final String add_server_send_info(String send_id,String user_answer_id, String regist_date){
        String sql = "INSERT INTO " + AiteaContract.SERVER_SEND_INFO.TABLE_NAME +
                " VALUES " + "(" +
                "\'" + send_id + "\'" + "," +
                "\'" + user_answer_id + "\'" + "," +
                "\'" + regist_date + "\'" + " )";
        return sql;
    }

    public static final String get_user_answer_id_count(){

        String sql = "SELECT " +
                "count(*) as " + AiteaContract.ANSWER_ARCHIVE_COUNT.ID  +
                " FROM " + AiteaContract.ANSWER_ARCHIVE.TABLE_NAME;

        return sql;
    }

    public static final String get_send_id_count(){

        String sql = "SELECT " +
                "count(*) as " + AiteaContract.SERVER_SEND_INFO_COUNT.ID  +
                " FROM " + AiteaContract.SERVER_SEND_INFO.TABLE_NAME;

        return sql;
    }

    public static final String increment_mistake_count(String q_id){
        String sql = "UPDATE " +
                AiteaContract.QUESTION_INFO.TABLE_NAME +
                " SET " + AiteaContract.QUESTION_INFO.MISTAKE_COUNT  +
                "= (" + AiteaContract.QUESTION_INFO.MISTAKE_COUNT + " + 1 )" +
                " WHERE " + AiteaContract.QUESTION_INFO.Q_ID + "=" + "\'" + q_id + "\'";
        return sql;
    }

    public static final String update_s_class_correct_rate(int field_code,int l_class_code,int m_class_code,int s_class_code,double[] s_class_molec_denomin,double s_class_correct_rate){
        String sql = "UPDATE " +
                AiteaContract.GENRE_INFO.TABLE_NAME +
                " SET " + AiteaContract.GENRE_INFO.S_CLASS_MOLECULE +
                "= " + s_class_molec_denomin[0]  +
                "," + AiteaContract.GENRE_INFO.S_CLASS_DENOMINATOR +
                "= " + s_class_molec_denomin[1]  +
                "," + AiteaContract.GENRE_INFO.S_CLASS_CORRECT_RATE  +
                "= " + s_class_correct_rate  +
                " WHERE " + AiteaContract.GENRE_INFO.FIELD_CODE + "=" + field_code +
                " AND " + AiteaContract.GENRE_INFO.L_CLASS_CODE + "=" + l_class_code +
                " AND " + AiteaContract.GENRE_INFO.M_CLASS_CODE + "=" + m_class_code +
                " AND " + AiteaContract.GENRE_INFO.S_CLASS_CODE + "=" + s_class_code;
        return sql;
    }

    public static final String get_all_genre_correct_rate(){
        String sql = "SELECT " +
                AiteaContract.GENRE_INFO.FIELD_CODE + "," +
                AiteaContract.GENRE_INFO.L_CLASS_CODE + "," +
                AiteaContract.GENRE_INFO.M_CLASS_CODE + "," +
                AiteaContract.GENRE_INFO.S_CLASS_CODE + "," +
                AiteaContract.GENRE_INFO.S_CLASS_CORRECT_RATE +
                " FROM " + AiteaContract.GENRE_INFO.TABLE_NAME;
        return sql;
    }


    public static final String get_all_genre_q_id_info(){
        String sql = "SELECT " +
                AiteaContract.QUESTION_INFO.Q_ID + "," +
                AiteaContract.QUESTION_INFO.FIELD_CODE + "," +
                AiteaContract.QUESTION_INFO.L_CLASS_CODE + "," +
                AiteaContract.QUESTION_INFO.M_CLASS_CODE + "," +
                AiteaContract.QUESTION_INFO.S_CLASS_CODE +
                " FROM " + AiteaContract.QUESTION_INFO.TABLE_NAME;
        return sql;
    }

    public static final String get_all_list_q_id_info(){
        String sql = "SELECT " +
                AiteaContract.QUESTION_INFO.Q_ID + "," +
                AiteaContract.QUESTION_INFO.YEAR + "," +
                AiteaContract.QUESTION_INFO.TURN + "," +
                AiteaContract.QUESTION_INFO.Q_NO + "," +
                AiteaContract.QUESTION_INFO.Q_NAME +
                " FROM " + AiteaContract.QUESTION_INFO.TABLE_NAME;
        return sql;
    }

    public static final String get_trial_exam_already_answer_recode(int set_q_id){
        String sql = "SELECT " +
                AiteaContract.TRIAL_EXAM_INFO.USER_ANSWER +
                " FROM " + AiteaContract.TRIAL_EXAM_INFO.TABLE_NAME +
                " WHERE " + AiteaContract.TRIAL_EXAM_INFO.SET_Q_ID + "=" + set_q_id;
        return sql;
    }
    public static final String get_trial_exam_already_answer_recode(){
        String sql = "SELECT " +
                AiteaContract.TRIAL_EXAM_INFO.USER_ANSWER +
                " FROM " + AiteaContract.TRIAL_EXAM_INFO.TABLE_NAME;
        return sql;
    }


    public static final String get_q_id_by_mistake_count(){
        String sql = "SELECT " +
                AiteaContract.QUESTION_INFO.Q_ID +
                " FROM " + AiteaContract.QUESTION_INFO.TABLE_NAME +
                " WHERE " + AiteaContract.QUESTION_INFO.OVERCOME_FLAG + "!=" + 1 +
                " AND " + AiteaContract.QUESTION_INFO.MISTAKE_COUNT + ">=" + 1 +
                " ORDER BY " + AiteaContract.QUESTION_INFO.MISTAKE_COUNT + " DESC";
        return sql;
    }

    public static final String add_trial_exam_info( int set_q_id,String q_id){

        String sql = "INSERT INTO " + AiteaContract.TRIAL_EXAM_INFO.TABLE_NAME +
                " ( " + AiteaContract.TRIAL_EXAM_INFO.SET_Q_ID + "," +
                AiteaContract.TRIAL_EXAM_INFO.Q_ID + "," +
                AiteaContract.TRIAL_EXAM_INFO.ELAPSED_TIME + " )" +
                " VALUES " + "(" +
                set_q_id + "," +
                "\'" + q_id + "\'" +  "," +
                0 +")";
        return sql;

    }

    public  static final String get_q_name(String q_id) {

        String sql = "SELECT " +
                AiteaContract.QUESTION_INFO.Q_NAME +
                " FROM " + AiteaContract.QUESTION_INFO.TABLE_NAME +
                " WHERE " + AiteaContract.QUESTION_INFO.Q_ID + "=" + "\'" + q_id + "\'";

        return sql;
    }

    public final static String get_correctAnswer(String q_id){
        String sql = "SELECT " +
                AiteaContract.QUESTION_INFO.CORRECT_ANSWER +
                " FROM " +  AiteaContract.QUESTION_INFO.TABLE_NAME +
                " WHERE " + AiteaContract.QUESTION_INFO.Q_ID + "=" + "\'" + q_id + "\'";
        return sql;
    }

    public final static String get_trial_exam_user_answer(int set_q_id){
        String sql = "SELECT " +
                AiteaContract.TRIAL_EXAM_INFO.USER_ANSWER +
                " FROM " +  AiteaContract.TRIAL_EXAM_INFO.TABLE_NAME +
                " WHERE " + AiteaContract.TRIAL_EXAM_INFO.SET_Q_ID + "=" + set_q_id;
        return sql;
    }
    public static final String update_trial_exam_info(int set_q_id,String user_answer,int correct_mistake,String user_answer_date,int elapsed_time){
        String sql = "UPDATE " +
                AiteaContract.TRIAL_EXAM_INFO.TABLE_NAME +
                " SET " + AiteaContract.TRIAL_EXAM_INFO.USER_ANSWER +
                "= " + "\'" + user_answer + "\'" +
                "," + AiteaContract.TRIAL_EXAM_INFO.CORRECT_MISTAKE +
                "= " + correct_mistake  +
                "," + AiteaContract.TRIAL_EXAM_INFO.USER_ANSWER_DATE  +
                "= "+ "\'" + user_answer_date + "\'" +
                "," + AiteaContract.TRIAL_EXAM_INFO.ELAPSED_TIME  +
                "= " + "(" + AiteaContract.TRIAL_EXAM_INFO.ELAPSED_TIME + " + " + elapsed_time  + ")" +
                " WHERE " + AiteaContract.TRIAL_EXAM_INFO.SET_Q_ID + "=" + set_q_id;
        return sql;
    }


    public final static String get_trial_exam_q_id(){
        String sql = "SELECT " +
                AiteaContract.TRIAL_EXAM_INFO.Q_ID +
                " FROM " +  AiteaContract.TRIAL_EXAM_INFO.TABLE_NAME;
        return sql;
    }

    public final static String get_trial_exam_info(){
        String sql = "SELECT " +
                AiteaContract.TRIAL_EXAM_INFO.Q_ID +  "," +
                AiteaContract.TRIAL_EXAM_INFO.USER_ANSWER + "," +
                AiteaContract.TRIAL_EXAM_INFO.CORRECT_MISTAKE +  "," +
                AiteaContract.TRIAL_EXAM_INFO.USER_ANSWER_DATE +  "," +
                AiteaContract.TRIAL_EXAM_INFO.ELAPSED_TIME +
                " FROM " +  AiteaContract.TRIAL_EXAM_INFO.TABLE_NAME;
        return sql;
    }

    public static final String add_performance_info(String peformance_id,String user_answer_date,int peformance){
        String sql = "INSERT INTO " + AiteaContract.PERFORMANCE_INFO.TABLE_NAME +
                " VALUES " + "(" +
                "\'" + peformance_id + "\'" + "," +
                "\'" + user_answer_date + "\'" + "," +
                peformance +  " )";
        return sql;
    }

    public static final String get_performance_id_count(){

        String sql = "SELECT " +
                "count(*) as " + AiteaContract.PERFORMANCE_INFO_COUNT.ID  +
                " FROM " + AiteaContract.PERFORMANCE_INFO.TABLE_NAME;

        return sql;
    }
    public static final String get_most_recent_set_q_id(){

        String sql = "SELECT " +
                AiteaContract.TRIAL_EXAM_INFO.SET_Q_ID +
                " FROM " +  AiteaContract.TRIAL_EXAM_INFO.TABLE_NAME +
                " ORDER BY " + AiteaContract.TRIAL_EXAM_INFO.USER_ANSWER_DATE + " DESC" +
                " LIMIT " + 1;
        return sql;
    }

    public static final String get_performance(int limit) {
        String sql = "SELECT " +
                AiteaContract.PERFORMANCE_INFO.PERFORMANCE + "," +
                AiteaContract.PERFORMANCE_INFO.USER_ANSWER_DATE +
                " FROM " + AiteaContract.PERFORMANCE_INFO.TABLE_NAME +
                " ORDER BY " + AiteaContract.PERFORMANCE_INFO.PERFORMANCE_ID + " DESC" +
                " LIMIT " + limit;

        return sql;
    }
	/**
     * 分野コード、分野名、小分類コード、小分類名、小分類の正答率を取得し、小分類の正答率を降順又は昇順で並べ替えるsql文を返す(limit:5)(order:asc or desc)
     * @return 作成したsql文
     */
    public static final String get_s_class_correct_rate(int limit, String order) {
        String sql = "SELECT " + "DISTINCT " +
                AiteaContract.GENRE_INFO.FIELD_CODE + "," +
                AiteaContract.GENRE_INFO.FIELD_NAME + "," +
                AiteaContract.GENRE_INFO.S_CLASS_CODE + "," +
                AiteaContract.GENRE_INFO.S_CLASS_NAME + "," +
                AiteaContract.GENRE_INFO.S_CLASS_CORRECT_RATE +
                " FROM " + AiteaContract.GENRE_INFO.TABLE_NAME +
                " WHERE " + AiteaContract.GENRE_INFO.S_CLASS_DENOMINATOR + " IS NOT NULL" +
                " ORDER BY " + AiteaContract.GENRE_INFO.S_CLASS_CORRECT_RATE + " " + order + " LIMIT " + limit;
        return sql;
    }

    /**
     * 分野コード、分野名、中分類コード、中分類名、中分類の正答率を取得し、中分類の正答率を降順又は昇順で並べ替えるsql文を返す(limit:5)(order:asc or desc)
     * @return 作成したsql文
     */
    public static final String get_m_class_correct_rate(int limit, String order) {
        String sql = "SELECT " +
                AiteaContract.GENRE_INFO.FIELD_CODE + "," +
                AiteaContract.GENRE_INFO.FIELD_NAME + "," +
                AiteaContract.GENRE_INFO.M_CLASS_CODE + "," +
                AiteaContract.GENRE_INFO.M_CLASS_NAME + "," +
                " ROUND((SUM(" + AiteaContract.GENRE_INFO.S_CLASS_MOLECULE + ") / SUM(" + AiteaContract.GENRE_INFO.S_CLASS_DENOMINATOR + ")), 1)" +
                " AS " + AiteaContract.GENRE_INFO.M_CLASS_CORRECT_RATE +
                " FROM " + AiteaContract.GENRE_INFO.TABLE_NAME +
                " WHERE " + AiteaContract.GENRE_INFO.S_CLASS_DENOMINATOR + " IS NOT NULL" +
                " GROUP BY " +  AiteaContract.GENRE_INFO.M_CLASS_CODE + "," + AiteaContract.GENRE_INFO.M_CLASS_NAME +
                " ORDER BY " + AiteaContract.GENRE_INFO.M_CLASS_CORRECT_RATE + " " + order + " LIMIT " + limit;
        return sql;
    }

    /**
     * 分野コード、分野名、大分類コード、大分類名、大分類の正答率を取得し、大分類の正答率を降順又は昇順で並べ替えるsql文を返す(limit:5)(order:asc or desc)
     * @return 作成したsql文
     */
    public static final String get_l_class_correct_rate(int limit, String order) {
        String sql = "SELECT " +
                AiteaContract.GENRE_INFO.FIELD_CODE + "," +
                AiteaContract.GENRE_INFO.FIELD_NAME + "," +
                AiteaContract.GENRE_INFO.L_CLASS_CODE + "," +
                AiteaContract.GENRE_INFO.L_CLASS_NAME + "," +
                " ROUND((SUM(" + AiteaContract.GENRE_INFO.S_CLASS_MOLECULE + ") / SUM(" + AiteaContract.GENRE_INFO.S_CLASS_DENOMINATOR + ")), 1)" +
                " AS " + AiteaContract.GENRE_INFO.L_CLASS_CORRECT_RATE +
                " FROM " + AiteaContract.GENRE_INFO.TABLE_NAME +
                " WHERE " + AiteaContract.GENRE_INFO.S_CLASS_DENOMINATOR + " IS NOT NULL" +
                " GROUP BY " +  AiteaContract.GENRE_INFO.L_CLASS_CODE + "," + AiteaContract.GENRE_INFO.L_CLASS_NAME +
                " ORDER BY " + AiteaContract.GENRE_INFO.L_CLASS_CORRECT_RATE + " " + order + " LIMIT " + limit;
        return sql;
    }

    /**
     * 分野コード、分野名、分野の正答率を取得するsql文を返す
     * @return 作成したsql文
     */
    public static final String get_field_correct_rate() {
        String sql = "SELECT " +
                AiteaContract.GENRE_INFO.FIELD_CODE + "," +
                AiteaContract.GENRE_INFO.FIELD_NAME + "," +
                " ROUND(((SUM(" + AiteaContract.GENRE_INFO.S_CLASS_MOLECULE + ") / SUM(" + AiteaContract.GENRE_INFO.S_CLASS_DENOMINATOR + ")) * 100), 1)" +
                " AS " + AiteaContract.GENRE_INFO.FIELD_CORRECT_RATE +
                " FROM " + AiteaContract.GENRE_INFO.TABLE_NAME +
                " WHERE " + AiteaContract.GENRE_INFO.S_CLASS_DENOMINATOR + " IS NOT NULL" +
                " GROUP BY " +  AiteaContract.GENRE_INFO.FIELD_CODE + "," + AiteaContract.GENRE_INFO.FIELD_NAME +
                " ORDER BY " + AiteaContract.GENRE_INFO.FIELD_CORRECT_RATE + " ASC ";
        return sql;
    }
}
