package com.support.qualifications.getqualifications;

import java.io.Serializable;

/**
 * tbl_tmp_save_infoのデータ型クラス
 */
public class tmp_save_info_data implements Serializable{

	private String tmp_save_id;
	private String elapsed_time;
	private int start_q_no;
	private String tmp_save_date;

}
