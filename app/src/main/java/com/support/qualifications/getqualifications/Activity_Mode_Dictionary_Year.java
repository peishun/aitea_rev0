package com.support.qualifications.getqualifications;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class Activity_Mode_Dictionary_Year extends Activity {

    private int mode_id;
    private Toolbar toolbar;
    private ListView myListview;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mode_dictionary_year);

        toolbar = (Toolbar) findViewById(R.id.toolbar_mode_dictionary_year);

        //リストビューの取得
        myListview = (ListView)findViewById(R.id.listview_dictionary_year);
        myListview.setEmptyView(findViewById(R.id.empty));

    }

    @Override
    protected void onResume() {
        super.onResume();

        Intent intent = getIntent();
        mode_id = intent.getIntExtra(IntentKeyword.MODE_ID, 0);

        toolbar.setTitle("年度一覧");
        toolbar.setTitleTextColor(Color.WHITE);

        //リストに表示するデータの配列
        ArrayList<User> users = new ArrayList<>();

        final question_info_data[] year_turn_data = getYear_turn_info();

        int icons = 0;

        //リストに表示するデータをセットする
        for (int i = 0; i < year_turn_data.length; i++) {
            User user = new User();
            user.setIcon(BitmapFactory.decodeResource(getResources(), icons));
            user.setYear_turn(year_turn_data[i].getYear() + " " + year_turn_data[i].getTurn());
            users.add(user);
        }

        //リストに表示するデータをアダプターにセット
        UserAdapter adapter = new UserAdapter(this, 0, users);

        myListview.setAdapter(adapter);

        //リストをタップしたときのEvent
        myListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(
                    AdapterView<?> parent,
                    View view, //タップされたビュー
                    int position, //何番目？
                    long id //View id
            ) {

                String[] q_id_data = null;

                q_id_data = getOut_q_id_data(
                        year_turn_data[position].getYear(),
                        year_turn_data[position].getTurn()
                );

                Intent intent;

                if(mode_id == AiteaContract.MODE.TRIALEXAM){

                    insert_trial_exam_info(q_id_data);

                    //オブジェクト生成及びintentに遷移先のパッケージ名とアクティビティ名を引数に設定
                    intent = new Intent(Activity_Mode_Dictionary_Year.this, Activity_Mode_TrialExam_List.class);

                    //遷移先に渡す値
                    intent.putExtra(IntentKeyword.Q_ID_LIST, q_id_data);

                    finish();


                }else{
                    //オブジェクト生成及びintentに遷移先のパッケージ名とアクティビティ名を引数に設定
                    intent = new Intent(Activity_Mode_Dictionary_Year.this, Activity_Mode_Dictionary_Question_List.class);

                    //遷移先に渡す値
                    intent.putExtra(IntentKeyword.Q_ID_LIST, q_id_data);

                }

                //intentの実行
                startActivity(intent);

            }
        });
        ///////////////////
        // toolbarの表示 //
        ///////////////////
    }

    private question_info_data[] getYear_turn_info(){

        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        /* Cursor(sql文実行結果を取得するもの)を生成し、sql文実行 */
        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_all_year_turn_info(), null);

        /* 行数文の大きさのデータクラス型配列 */
        question_info_data[] out_q_i_data = new question_info_data[c.getCount()];

        int i = 0;
        /* sqlで実行した結果取得したテーブルを1行ずつ参照 */
        while (c.moveToNext()){
            out_q_i_data[i] = new question_info_data();
            out_q_i_data[i].setYear(c.getString(c.getColumnIndex(AiteaContract.QUESTION_INFO.YEAR)));
            out_q_i_data[i].setTurn(c.getString(c.getColumnIndex(AiteaContract.QUESTION_INFO.TURN)));
            i++;
        }
        c.close();
        db.close();

        return out_q_i_data;
    }

    private String[] getOut_q_id_data(String year, String turn){

        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        /* Cursor(sql文実行結果を取得するもの)を生成し、sql文実行 */
        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_q_id(year, turn), null);

        if(c.getCount() == 0){
            return null;
        }

        /* 行数文の大きさのデータクラス型配列 */
        String[] out_q_i_data = new String[c.getCount()];

        int i = 0;
        /* sqlで実行した結果取得したテーブルを1行ずつ参照 */
        while (c.moveToNext()){
            out_q_i_data[i] = new String();
            out_q_i_data[i] = c.getString(c.getColumnIndex(AiteaContract.QUESTION_INFO.Q_ID));
            i++;
        }
        c.close();
        db.close();

        return out_q_i_data;
    }

    private void insert_trial_exam_info(String[] q_id_data){
        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        for( int i = 0; i < q_id_data.length ; i++ ) {
            db.execSQL(DB_makesql.add_trial_exam_info( i + 1 , q_id_data[i]));
        }

        db.close();
    }

    public class UserAdapter extends ArrayAdapter<User> {

        private LayoutInflater layoutInflater;

        public UserAdapter(Context c, int id, ArrayList<User> users) {
            super(c, id, users);
            this.layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        /* リストをスクロールして、新たにリストが表示されるときに呼ばれる */
        @Override
        public View getView(int pos, View convertview, ViewGroup parent){

            ViewHolder holder;

            /* 再利用されるビューが無い場合、 */
            if( convertview == null ){
                convertview = layoutInflater.inflate(
                        R.layout.year_list_item,
                        parent,
                        false
                );
                holder = new ViewHolder();
                holder.icon = (ImageView) convertview.findViewById(R.id.yearicon);
                holder.year_turn = (TextView) convertview.findViewById(R.id.year_turn_info);
                convertview.setTag(holder);//データ保持
            }else{
                holder = (ViewHolder)convertview.getTag();
            }

            /* Userデータを取ってきて表示 */
            User user = (User)getItem(pos);

            holder.icon.setImageBitmap(user.getIcon());
            holder.year_turn.setText(user.getYear_turn());

            return convertview;

        }
    }

    //リストに表示するデータ定義
    static class ViewHolder{
        ImageView icon;
        TextView year_turn;
    }

    //リストに表示するデータ型クラス
    public class User{
        private Bitmap icon;
        private String year_turn;

        public void setIcon(Bitmap icon) {
            this.icon = icon;
        }

        public void setYear_turn(String year_turn) {
            this.year_turn = year_turn;
        }

        public Bitmap getIcon() {

            return icon;
        }

        public String getYear_turn() {
            return year_turn;
        }

    }
}