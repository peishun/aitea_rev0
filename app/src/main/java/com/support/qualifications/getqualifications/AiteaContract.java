package com.support.qualifications.getqualifications;

import android.provider.BaseColumns;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by joho on 2015/12/11.
 */
public final class AiteaContract {

    public static final String MY_EXAM_CATEGORY = "AP";

    public AiteaContract() {
    }

    public static final String getNowDate(){
        String nowtime;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        return df.format(date);
    }

    public static abstract  class POST_DATA implements BaseColumns{

        public static final String EXAM_CATEGORY = "exam_category";
        public static final String Q_ID = "question_id";
        public static final String USER_ANSWER = "user_answer";
        public static final String CORRECT_MISTAKE = "correct_mistake";
    }
    public static abstract class QUESTION_INFO implements BaseColumns {

        public static final String TABLE_NAME = "tbl_question_info";
        public static final String Q_ID = "question_id";
        public static final String YEAR = "year";
        public static final String TURN = "turn";
        public static final String Q_NO = "question_no";
        public static final String FIELD_CODE = "field_code";
        public static final String L_CLASS_CODE = "large_class_code";
        public static final String M_CLASS_CODE = "middle_class_code";
        public static final String S_CLASS_CODE = "small_class_code";
        public static final String Q_NAME = "question_name";
        public static final String Q_SENTENCE = "question_sentence";
        public static final String Q_IMAGE = "question_image";
        public static final String SELECTION_SENTENCE_A = "selection_sentence_a";
        public static final String SELECTION_SENTENCE_I = "selection_sentence_i";
        public static final String SELECTION_SENTENCE_U = "selection_sentence_u";
        public static final String SELECTION_SENTENCE_E = "selection_sentence_e";
        public static final String SELECTION_IMAGE = "selection_image";
        public static final String CORRECT_ANSWER = "correct_answer";
        public static final String Q_CORRECT_RATE = "question_correct_rate";
        public static final String MISTAKE_COUNT = "mistake_count";
        public static final String FAVORITE_FLAG = "favorite_flag";
        public static final String OVERCOME_FLAG = "overcome_flag";
        public static final String SELECTION_RATE_A = "selection_rate_a";
        public static final String SELECTION_RATE_I = "selection_rate_i";
        public static final String SELECTION_RATE_U = "selection_rate_u";
        public static final String SELECTION_RATE_E = "selection_rate_e";
        public static final String SELECTION_RATE_Q = "selection_rate_q";
    }

    public static abstract class GENRE_INFO implements BaseColumns {

        public static final String TABLE_NAME = "tbl_genre_info";
        public static final String FIELD_CODE = "field_code";
        public static final String FIELD_NAME = "field_name";
        public static final String L_CLASS_CODE = "large_class_code";
        public static final String L_CLASS_NAME = "large_class_name";
        public static final String M_CLASS_CODE = "middle_class_code";
        public static final String M_CLASS_NAME = "middle_class_name";
        public static final String S_CLASS_CODE = "small_class_code";
        public static final String S_CLASS_NAME = "small_class_name";
        public static final String S_CLASS_DENOMINATOR = "small_class_denominator";
        public static final String S_CLASS_MOLECULE = "small_class_molecule";
        public static final String S_CLASS_CORRECT_RATE = "small_class_correct_rate";
        public static final String M_CLASS_CORRECT_RATE = "middle_class_correct_rate";
        public static final String L_CLASS_CORRECT_RATE = "large_class_correct_rate";
        public static final String FIELD_CORRECT_RATE = "large_class_correct_rate";

    }

    public static abstract class GENRE_INFO_COUNT implements BaseColumns{
        public static final String RAW = "raw_count";
    }

    public static abstract class ANSWER_ARCHIVE implements BaseColumns {

        public static final String TABLE_NAME = "tbl_answer_archive";
        public static final String USER_ANSWER_ID = "user_answer_id";
        public static final String Q_ID = "question_id";
        public static final String USER_ANSWER = "user_answer";
        public static final String CORRECT_MISTAKE = "correct_mistake";
        public static final String USER_ANSWER_DATE = "user_answer_date";
    }

    public static abstract class ANSWER_ARCHIVE_COUNT implements BaseColumns{
        public static final String ID = "id_count";
    }

    public static abstract class SERVER_SEND_INFO implements BaseColumns {

        public static final String TABLE_NAME = "tbl_server_send_info";
        public static final String SEND_ID = "send_id";
        public static final String USER_ANSWER_ID = "user_answer_id";
        public static final String REGIST_DATE = "regist_date";
    }

    public static abstract class SERVER_SEND_INFO_COUNT implements BaseColumns{
        public static final String ID = "id_count";
    }

    public static abstract class PERFORMANCE_INFO implements BaseColumns {

        public static final String TABLE_NAME = "tbl_performance_info";
        public static final String PERFORMANCE_ID = "performance_id";
        public static final String USER_ANSWER_DATE = "user_answer_date";
        public static final String PERFORMANCE = "performance";
    }
    public static abstract class PERFORMANCE_INFO_COUNT implements BaseColumns{
        public static final String ID = "id_count";
    }

    public static abstract class TMP_SAVE_INFO implements BaseColumns {

        public static final String TABLE_NAME = "tbl_tmp_save_info";
        public static final String TMP_SAVE_ID = "tmp_save_id";
        public static final String ELAPSED_TIME = "elapsed_time";
        public static final String START_Q_NO = "start_question_no";
        public static final String TMP_SAVE_DATE = "tmp_save_date";
    }

    public static abstract class TRIAL_EXAM_INFO implements BaseColumns {

        public static final String TABLE_NAME = "tbl_trial_exam_info";
        public static final String SET_Q_ID = "set_question_id";
        public static final String Q_ID = "question_id";
        public static final String USER_ANSWER = "user_answer";
        public static final String CORRECT_MISTAKE = "correct_mistake";
        public static final String USER_ANSWER_DATE = "user_answer_date";
        public static final String ELAPSED_TIME = "elapsed_time";
    }

    public static abstract class MODE{
        public static final int AITEA = 1;
        public static final int TRIALEXAM = 2;
        public static final int DICTIONARY = 3;
        public static final int REVIEW = 4;
    }
}
