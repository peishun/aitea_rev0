package com.support.qualifications.getqualifications;

import java.io.Serializable;

/**
 * tbl_server_send_infoのデータ型クラス
 */
public class server_send_info_data implements Serializable{

	private String send_id;
	private String user_answer_id;
	private String regist_date;

}
