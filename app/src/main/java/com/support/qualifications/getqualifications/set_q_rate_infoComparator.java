package com.support.qualifications.getqualifications;

/**
 * Created by joho on 2015/12/28.
 */
public class set_q_rate_infoComparator implements java.util.Comparator {

    @Override
    public int compare(Object lhs, Object rhs) {
        if(((q_rate_info)lhs).getCorrect_rate() > ((q_rate_info) rhs).getCorrect_rate()){
            return 1;
        }
        else if(((q_rate_info)lhs).getCorrect_rate() < ((q_rate_info) rhs).getCorrect_rate()){
            return -1;
        }
        return 0;
    }
}