package com.support.qualifications.getqualifications;

/**
 * Created by joho on 2015/12/28.
 */
public class q_rate_info {

    private int rank;
    private int field_code;

    public void setField_code(int field_code) {
        this.field_code = field_code;
    }

    public void setL_class_code(int l_class_code) {
        this.l_class_code = l_class_code;
    }

    public void setM_class_code(int m_class_code) {
        this.m_class_code = m_class_code;
    }

    public void setS_class_code(int s_class_code) {
        this.s_class_code = s_class_code;
    }

    public void setCorrect_rate(double correct_rate) {
        this.correct_rate = correct_rate;
    }

    private int l_class_code;
    private int m_class_code;
    private int s_class_code;
    private int rank_repetedCount;
    private double correct_rate;
    private double set_q_rate;

    q_rate_info(){
        this.rank = 1;
        this.rank_repetedCount = 1;
    }

    public double getCorrect_rate() {
        return correct_rate;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public void setRank_repetedCount(int rank_repetedCount) {
        this.rank_repetedCount = rank_repetedCount;
    }
    public int getRank_repetedCount() {
        return rank_repetedCount;
    }

    public int getRank() {
        return rank;
    }
    public void setSet_q_rate(double set_q_rate) {
        this.set_q_rate = set_q_rate;
    }

    public double getSet_q_rate() {
        return set_q_rate;
    }
    public int getField_code() {
        return field_code;
    }

    public int getL_class_code() {
        return l_class_code;
    }

    public int getM_class_code() {
        return m_class_code;
    }

    public int getS_class_code() {
        return s_class_code;
    }

}
