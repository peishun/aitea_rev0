package com.support.qualifications.getqualifications;

import java.io.Serializable;

/**
 * tbl_performance_infoのデータ型クラス
 */
public class performance_info_data implements Serializable{

	public String getPerformance_id() {
		return performance_id;
	}

	public void setPerformance_id(String performance_id) {
		this.performance_id = performance_id;
	}

	public String getUser_answer_date() {
		return user_answer_date;
	}

	public void setUser_answer_date(String user_answer_date) {
		this.user_answer_date = user_answer_date;
	}

	public int getPerformance() {
		return performance;
	}

	public void setPerformance(int performance) {
		this.performance = performance;
	}

	private String performance_id;
	private String user_answer_date;
	private int performance;

}
