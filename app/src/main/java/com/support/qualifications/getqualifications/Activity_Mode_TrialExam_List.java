package com.support.qualifications.getqualifications;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class Activity_Mode_TrialExam_List extends Activity {

    private String[] q_id_data;
    private Toolbar toolbar;
    private ListView myListview;

    public static Finishflag Fflag = new Finishflag();

    public static final String SHOWMARKALERTFLAG = "com.support.qualifications.getqualifications.SHOWMARKALERTFLAG";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mode_trial_exam_list);


        setShowAlertFlag(true);

        ///////////////////
        // toolbarの表示 //
        ///////////////////
        toolbar = (Toolbar) findViewById(R.id.toolbar_mode_trialexam_list);
        toolbar.inflateMenu(R.menu.trialexam_list);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    // "採点"ボタン
                    // ボタン押下後、Activity_Question_Resultに遷移
                    case R.id.TrialList_mark:
                        goActivity_Question_Result();
                        break;
                }
                return true;
            }
        });

        //リストビューの取得
        myListview = (ListView)findViewById(R.id.listview_mode_trialexam_list);

        myListview.setEmptyView(findViewById(R.id.empty));

        Intent intent = getIntent();
        q_id_data =  intent.getStringArrayExtra(IntentKeyword.Q_ID_LIST);

        if(!isExistSet_q_id()) {
            // オブジェクト生成及びintentに遷移先のパッケージ名とアクティビティ名を引数に設定
            Intent intentfirst = new Intent(Activity_Mode_TrialExam_List.this, Activity_Question.class);
            intentfirst.putExtra(IntentKeyword.SELECT_Q_ID, 0);
            intentfirst.putExtra(IntentKeyword.Q_ID_LIST, q_id_data);
            intentfirst.putExtra(IntentKeyword.MODE_ID, AiteaContract.MODE.TRIALEXAM);
            startActivity(intentfirst);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        toolbar.setTitle("年度名を表示");
        toolbar.setTitleTextColor(Color.WHITE);

        //リストに表示するデータの配列
        ArrayList<User> users = new ArrayList<>();

        Boolean[] isAlredy_answers = get_trial_alredy_answer(q_id_data.length);

        String[] q_names = get_q_names(q_id_data);

        int icons = R.mipmap.ic_launcher;

        //リストに表示するデータをセットする
        for (int i = 0; i < q_names.length; i++) {
            User user = new User();
            user.setIcon(BitmapFactory.decodeResource(getResources(), icons));
            user.setQ_info("問" + (i + 1));
            user.setQ_summary(q_names[i]);
            user.setIsAnswered(isAlredy_answers[i]);
            users.add(user);
        }

        //リストに表示するデータをアダプターにセット
        UserAdapter adapter = new UserAdapter(this, 0, users);

        myListview.setAdapter(adapter);

        //リストをタップしたときのEvent
        myListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(
                    AdapterView<?> parent,
                    View view, //タップされたビュー
                    int position, //何番目？
                    long id //View id
            ) {

                // オブジェクト生成及びintentに遷移先のパッケージ名とアクティビティ名を引数に設定
                Intent intent = new Intent(Activity_Mode_TrialExam_List.this, Activity_Question.class);

                intent.putExtra(IntentKeyword.SELECT_Q_ID, position);
                intent.putExtra(IntentKeyword.Q_ID_LIST, q_id_data);
                intent.putExtra(IntentKeyword.MODE_ID, AiteaContract.MODE.TRIALEXAM);

                // intentの実行
                startActivity(intent);

            }
        });

        if(getShowAlertFlag()) {
            if (isExistAnswerRecode()) {
                alertshow();
            }
        }


    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if(Fflag.getFlag()){
            finish();
        }
    }

    private Boolean[] get_trial_alredy_answer(int numberOfQuestion){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Boolean[] isAlredy_answers = new Boolean[numberOfQuestion];

        for( int i = 0; i < numberOfQuestion; i++ ){
            Cursor c = null;
            c = db.rawQuery(DB_makesql.get_trial_exam_already_answer_recode(i+1), null);
            isAlredy_answers[i] = new Boolean(false);

            if (c.moveToNext()) {
                if( c.getString(c.getColumnIndex(AiteaContract.TRIAL_EXAM_INFO.USER_ANSWER)) != null ) {
                    isAlredy_answers[i] = true;
                }
            }

            c.close();
        }

        db.close();

        return isAlredy_answers;
    }
    private String[] get_q_names(String[] q_id_data){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        String[] out_q_name = new String[q_id_data.length];

        for( int i = 0; i < q_id_data.length; i++ ){
            Cursor c = null;
            c = db.rawQuery(DB_makesql.get_q_name(q_id_data[i]), null);
            if (c.moveToNext()) {
                out_q_name[i] = new String();
                out_q_name[i] = c.getString(c.getColumnIndex(AiteaContract.QUESTION_INFO.Q_NAME));
            }
            c.close();
        }
        db.close();

        return out_q_name;
    }

    public class UserAdapter extends ArrayAdapter<User> {

        private LayoutInflater layoutInflater;

        public UserAdapter(Context c, int id, ArrayList<User> users) {
            super(c, id, users);
            this.layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        /* リストをスクロールして、新たにリストが表示されるときに呼ばれる */
        @Override
        public View getView(int pos, View convertview, ViewGroup parent){

            ViewHolder holder;

            /* 再利用されるビューが無い場合、 */
            if( convertview == null ){
                convertview = layoutInflater.inflate(
                        R.layout.trial_exam_list_item,
                        parent,
                        false
                );
                holder = new ViewHolder();
                holder.icon = (ImageView) convertview.findViewById(R.id.trial_exam_questionicon);
                holder.info = (TextView) convertview.findViewById(R.id.trial_exam_questionInfo);
                holder.summary = (TextView) convertview.findViewById(R.id.trial_exam_questionsummary);
                holder.alreadt_answer = (ImageView) convertview.findViewById(R.id.trial_exam_alredy_answer);

                convertview.setTag(holder);//データ保持
            }else{
                holder = (ViewHolder)convertview.getTag();
            }

            /* Userデータを取ってきて表示 */
            User user = (User)getItem(pos);

            holder.icon.setImageBitmap(user.getIcon());
            holder.info.setText(user.getQ_info());
            holder.summary.setText(user.getQ_summary());
            if(user.getIsAnswered()) {
                holder.alreadt_answer.setImageResource(R.drawable.check_mark);
            }else{
                holder.alreadt_answer.setImageResource(0);
            }

            return convertview;

        }
    }

    //リストに表示するデータ定義
    static class ViewHolder{
        ImageView icon;
        TextView info;
        TextView summary;
        ImageView alreadt_answer;
    }

    //リストに表示するデータ型クラス
    private class User{
        private Bitmap icon;
        private String q_info;
        private String q_summary;
        private Boolean isAnswered;

        public void setIcon(Bitmap icon) {
            this.icon = icon;
        }

        public void setQ_info(String q_info) {
            this.q_info = q_info;
        }

        public void setQ_summary(String q_summary) {
            this.q_summary = q_summary;
        }

        public void setIsAnswered(Boolean isAnswered) {
            this.isAnswered = isAnswered;
        }

        public Bitmap getIcon() {
            return icon;
        }

        public String getQ_info() {
            return q_info;
        }

        public String getQ_summary() {
            return q_summary;
        }

        public Boolean getIsAnswered() {
            return isAnswered;
        }

    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        // Keyボタン(BACKボタン)押下時の処理
        if(keyCode == KeyEvent.KEYCODE_BACK){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            // アラートダイアログのタイトルを設定します
            alertDialogBuilder.setTitle("模擬試験モード中断");
            // アラートダイアログのメッセージを設定します
            alertDialogBuilder.setMessage("回答内容保存しますか？");
            // アラートダイアログの肯定ボタンがクリックされた時に呼び出されるコールバックリスナーを登録します
            alertDialogBuilder.setPositiveButton("はい", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            // アラートダイアログの否定ボタンがクリックされた時に呼び出されるコールバックリスナーを登録します
            alertDialogBuilder.setNegativeButton("いいえ", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    delete_trial_exam_info();
                    finish();
                }
            });

            // アラートダイアログのキャンセルが可能かどうかを設定します
            alertDialogBuilder.setCancelable(true);
            AlertDialog alertDialog = alertDialogBuilder.create();
            // アラートダイアログを表示します
            alertDialog.show();

            return true;
        }
        else{
            // デフォルトの戻るボタン押下時の処理と同じ(ひとつ前の画面に戻る)
            return super.onKeyDown(keyCode, event);
        }
    }

    private void delete_trial_exam_info(){

        /* DatabaseとOpenHelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

            /* sql文実行 */
        db.delete(AiteaContract.TRIAL_EXAM_INFO.TABLE_NAME, null, null);
        db.close();
    }

    private boolean isExistSet_q_id(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_most_recent_set_q_id(), null);

        Boolean ret = false;
        if (c.moveToNext()) {
            ret = true;
        }

        c.close();
        db.close();

        return ret;
    }

    private boolean isExistAnswerRecode(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_trial_exam_already_answer_recode(), null);

        while (c.moveToNext()) {
            if( c.getString(c.getColumnIndex(AiteaContract.TRIAL_EXAM_INFO.USER_ANSWER)) == null ) {
                c.close();
                db.close();
                return false;
            }
        }

        c.close();
        db.close();

        return true;
    }
    private void alertshow(){

        setShowAlertFlag(false);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        // アラートダイアログのタイトルを設定します
        alertDialogBuilder.setTitle("全問題の回答が完了しました");
        // アラートダイアログのメッセージを設定します
        alertDialogBuilder.setMessage("採点しますか？");
        // アラートダイアログの肯定ボタンがクリックされた時に呼び出されるコールバックリスナーを登録します
        alertDialogBuilder.setPositiveButton("はい", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                goActivity_Question_Result();
            }
        });
        // アラートダイアログの否定ボタンがクリックされた時に呼び出されるコールバックリスナーを登録します
        alertDialogBuilder.setNegativeButton("いいえ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        // アラートダイアログを表示します
        alertDialog.show();

    }
    private void goActivity_Question_Result(){

        // オブジェクト生成
        Intent intent = new Intent(Activity_Mode_TrialExam_List.this, Activity_Question_Result.class);

        Activity_Mode_TrialExam_List.Fflag.setFlag(true);

        // intentの実行
        startActivity(intent);

        finish();
    }

    private void setShowAlertFlag(boolean bool){

        SharedPreferences pref = getSharedPreferences("AiteaPref", MODE_PRIVATE);
        SharedPreferences.Editor e = pref.edit();
        e.putBoolean(SHOWMARKALERTFLAG, bool);
        e.commit();
    }

    private boolean getShowAlertFlag(){
        SharedPreferences pref = getSharedPreferences("AiteaPref", MODE_PRIVATE);
        return pref.getBoolean(SHOWMARKALERTFLAG, false);

    }

}

