package com.support.qualifications.getqualifications;

import android.app.Activity;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class Activity_Main extends Activity {

    private ArrayList<server_send_data> dataList = new ArrayList<server_send_data>();

    private String[] q_id_data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        send_to_server();

        ////////////////////////////
        // ボタン押下後、画面遷移 //
        ////////////////////////////

        // aiteaボタン
        findViewById(R.id.mode_aitea).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String[] q_id_data = get_q_id_by_q_rate();

                // オブジェクト生成
                Intent intent = new Intent(Activity_Main.this,Activity_Question.class);
                // 遷移先に渡す値
                intent.putExtra(IntentKeyword.MODE_ID, AiteaContract.MODE.AITEA);
                intent.putExtra(IntentKeyword.Q_ID_LIST, q_id_data);
                intent.putExtra(IntentKeyword.SELECT_Q_ID, 0);
                // intentの実行
                startActivity(intent);
            }
        });
        // 模擬試験ボタン
        findViewById(R.id.mode_mockexam).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Activity_Mode_TrialExam_List.Fflag.setFlag(false);
               q_id_data = get_trial_exam_q_id();

                if(q_id_data == null){
                    goDictionary_Year();

                }else{
                    showAlertDialog();
                }
            }
        });
        // 問題辞書ボタン
        findViewById(R.id.mode_dictionary).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // オブジェクト生成
                Intent intent = new Intent(Activity_Main.this,Activity_Mode_Dictionary.class);
                // 遷移先に渡す値
                intent.putExtra(IntentKeyword.MODE_ID, AiteaContract.MODE.DICTIONARY);
                // intentの実行
                startActivity(intent);
            }
        });
        // 復習ボタン
        findViewById(R.id.mode_review).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String[] q_id_data = get_q_id_by_mistake_count();

                // オブジェクト生成及びintentに遷移先のパッケージ名とアクティビティ名を引数に設定
                Intent intent = new Intent(Activity_Main.this, Activity_Mode_Dictionary_Question_List.class);

                //遷移先に渡す値
                intent.putExtra(IntentKeyword.Q_ID_LIST, q_id_data);
                intent.putExtra(IntentKeyword.MODE_ID, AiteaContract.MODE.REVIEW);

                // intentの実行
                startActivity(intent);
            }
        });

        ///////////////////
        // toolbarの表示 //
        ///////////////////

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        toolbar.setTitle("aitea");
        toolbar.setTitleTextColor(Color.WHITE);

        toolbar.inflateMenu(R.menu.main);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                boolean ret = true;
                switch (item.getItemId()) {

                    ////////////////////////////
                    // ボタン押下後、画面遷移 //
                    ////////////////////////////

                    // メニュー内の"成績表示"押下後の処理
                    case R.id.menu_trialexamresult:

                        // オブジェクト生成
                        Intent intent1 = new Intent(Activity_Main.this,Activity_OptionMenu_TrialExamResult.class);
                        // intentの実行
                        startActivity(intent1);

                        ret = true;
                        break;

                    // メニュー内の"成績表示"押下後の処理
                    case R.id.menu_performance:

                        // オブジェクト生成
                        Intent intent2 = new Intent(Activity_Main.this,Activity_OptionMenu_Performance.class);
                        // intentの実行
                        startActivity(intent2);

                        ret = true;
                        break;

                    // メニュー内の"設定"押下後の処理
                    case R.id.menu_setting:

                        // オブジェクト生成
                        Intent intent3 = new Intent(Activity_Main.this,Activity_OptionMenu_Setting.class);
                        // intentの実行
                        startActivity(intent3);

                        ret = true;
                        break;

                    // メニュー内の"更新の確認"押下後の処理
                    case R.id.menu_update:
//                        // オブジェクト生成
//                        Intent intent4 = new Intent(Activity_Main.this,Activity_Question.class);
//                        // intentの実行
//                        startActivity(intent4);
                        ret = true;
                        break;
                }
                return ret;
            }
        });
    }
    private void delete_trial_exam_info(){

        /* DatabaseとOpenHelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

            /* sql文実行 */
        db.delete(AiteaContract.TRIAL_EXAM_INFO.TABLE_NAME, null, null);
        db.close();
    }
    private void showAlertDialog(){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        // アラートダイアログのタイトルを設定します
        alertDialogBuilder.setTitle("模擬試験モード再開");
        // アラートダイアログのメッセージを設定します
        alertDialogBuilder.setMessage("前回の続きからはじめますか？");
        // アラートダイアログの肯定ボタンがクリックされた時に呼び出されるコールバックリスナーを登録します
        alertDialogBuilder.setPositiveButton("はい", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                goTrialExam_List();
            }
        });
        // アラートダイアログの否定ボタンがクリックされた時に呼び出されるコールバックリスナーを登録します
        alertDialogBuilder.setNegativeButton("いいえ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                delete_trial_exam_info();
                goDictionary_Year();
            }
        });

        // アラートダイアログのキャンセルが可能かどうかを設定します
        alertDialogBuilder.setCancelable(true);
        AlertDialog alertDialog = alertDialogBuilder.create();
        // アラートダイアログを表示します
        alertDialog.show();
    }

    private void goTrialExam_List(){
        // オブジェクト生成
        Intent intent = new Intent(Activity_Main.this,Activity_Mode_TrialExam_List.class);

        // 遷移先に渡す値
        intent.putExtra(IntentKeyword.MODE_ID, AiteaContract.MODE.TRIALEXAM);
        intent.putExtra(IntentKeyword.Q_ID_LIST, q_id_data);
        // intentの実行
        startActivity(intent);
    }


    private void goDictionary_Year(){
        // オブジェクト生成
        Intent intent = new Intent(Activity_Main.this,Activity_Mode_Dictionary_Year.class);
        // 遷移先に渡す値
        intent.putExtra(IntentKeyword.MODE_ID, AiteaContract.MODE.TRIALEXAM);
        // intentの実行
        startActivity(intent);
    }

    private String[] get_trial_exam_q_id(){

        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        /* Cursor(sql文実行結果を取得するもの)を生成し、sql文実行 */
        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_trial_exam_q_id(), null);

        String[] q_id_data = new String[c.getCount()];

        int i = 0;
        /* sqlで実行した結果取得したテーブルを1行ずつ参照 */
        while (c.moveToNext()){
            q_id_data[i] = new String();
            q_id_data[i] = c.getString(c.getColumnIndex(AiteaContract.TRIAL_EXAM_INFO.Q_ID));
            i++;
        }
        if(i == 0){
            return  null;
        }

        c.close();
        db.close();

        return q_id_data;
    }
    private String[] get_q_id_by_q_rate(){

        /* 全てのジャンルの正答率を取得する */
        ArrayList<q_rate_info> q_rate_infos = get_all_genre_correct_rate();

        /* 正答率が低いもの順にソート */
        Collections.sort(q_rate_infos, new set_q_rate_infoComparator());

        /* 各ジャンルに正答率についての順位を付ける(正答率が低いものが順位が高い) */
        calc_rank(q_rate_infos);

        /* 出題確率を計算する */
        calc_set_q_rate_infos(q_rate_infos);

        /* 出題確率を元に問題idを取得する */
        return select_q_id_by_q_rate(q_rate_infos);
    }

    private HashMap<String,ArrayList<String>> get_all_q_id_info(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        HashMap<String,ArrayList<String>> map = new HashMap<>();

        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_all_genre_q_id_info(), null);

        while (c.moveToNext()) {
            // keyをつくる
            String key = String.valueOf(c.getInt(c.getColumnIndex(AiteaContract.QUESTION_INFO.FIELD_CODE))) + "_" +
                            String.valueOf(c.getInt(c.getColumnIndex(AiteaContract.QUESTION_INFO.L_CLASS_CODE))) + "_" +
                            String.valueOf(c.getInt(c.getColumnIndex(AiteaContract.QUESTION_INFO.M_CLASS_CODE))) + "_" +
                            String.valueOf(c.getInt(c.getColumnIndex(AiteaContract.QUESTION_INFO.S_CLASS_CODE)));

            String q_id = c.getString(c.getColumnIndex(AiteaContract.QUESTION_INFO.Q_ID));

            // 初めて追加されるキー
            if(map.get(key) == null){

                //ArrayList新規作成
                ArrayList<String> list = new ArrayList<>();
                list.add(q_id);

                // マップ追加
                map.put(key, list);

            } else {
                // 既にあるキー
                map.get(key).add(q_id);
            }

        }

        c.close();
        db.close();

        return map;

    }


    private String get_q_id( HashMap<String,ArrayList<String>> map ,int field_code,int l_class_code,int m_class_code,int s_class_code){

        String key = String.valueOf(field_code) + "_" +
                String.valueOf(l_class_code) + "_" +
                String.valueOf(m_class_code) + "_" +
                String.valueOf(s_class_code);

        Random rnd = new Random();

        if(map.get(key).size() == 0) {
            return null;
        }

        int rand_index = rnd.nextInt(map.get(key).size());

        String q_id = map.get(key).get(rand_index);

        map.get(key).remove(rand_index);

        return q_id;
    }
    /**
     * 各小ジャンルがもつ確率を元に、出題する問題の問題idを取得する。
     * @param q_rate_infos
     * @return
     */
    private String[] select_q_id_by_q_rate(ArrayList<q_rate_info> q_rate_infos){

        final int GET_QUESTION_NUM = 100;

        HashMap<String,ArrayList<String>> map = get_all_q_id_info();

        String[] q_id_data = new String[GET_QUESTION_NUM];

        int i = 0;
        double q_rate_ofset = 0;

        while( i < GET_QUESTION_NUM ) {

            double sum_q_rate = 0 + q_rate_ofset;
            double rand = Math.random();

            for (int j = 0; j < q_rate_infos.size(); j++) {

                double set_q_rate = q_rate_infos.get(j).getSet_q_rate();

                if ( ( rand >= sum_q_rate ) && ( rand < ( sum_q_rate + set_q_rate ) ) ) {

                    String get_q_id = get_q_id( map,q_rate_infos.get(j).getField_code(),q_rate_infos.get(j).getL_class_code(),q_rate_infos.get(j).getM_class_code(),q_rate_infos.get(j).getS_class_code());

                    if( get_q_id == null) {
                        q_rate_ofset += q_rate_infos.get(j).getSet_q_rate();
                        q_rate_infos.remove(j);
                        break;
                    }
                    else{
                        q_id_data[i] = get_q_id;
                        i++;
                        break;
                    }

                } else {
                    sum_q_rate += set_q_rate;
                }
            }
        }

        return q_id_data;
    }

    /** 出題
     *
     * @param q_rate_infos
     */
    private void calc_set_q_rate_infos(ArrayList<q_rate_info> q_rate_infos){

        final int t = q_rate_infos.get(q_rate_infos.size()- 1).getRank();;
        int r = t;
        int count_r = q_rate_infos.get(q_rate_infos.size() - 1).getRank_repetedCount();

        for( int i = q_rate_infos.size() - 1; i > 0; i-- ){

            q_rate_infos.get(i).setSet_q_rate(Analyze_Calc.set_q_rate_infos(r,count_r,t));
            q_rate_infos.get(i).setRank_repetedCount(count_r);

            if( q_rate_infos.get(i).getRank() != q_rate_infos.get(i - 1).getRank() ) {
                r = q_rate_infos.get(i - 1).getRank();
                count_r = q_rate_infos.get(i - 1).getRank_repetedCount();
            }
        }
        q_rate_infos.get(0).setSet_q_rate(Analyze_Calc.set_q_rate_infos(r, count_r, t));
        q_rate_infos.get(0).setRank_repetedCount(count_r);

    }

    private void calc_rank(ArrayList<q_rate_info> q_rate_infos){

        int rank = 1;
        int rank_repetedCount = 1;

        for( int i = 1; i < q_rate_infos.size(); i++ ){

            if( q_rate_infos.get(i - 1).getCorrect_rate() == q_rate_infos.get(i).getCorrect_rate() ){
                rank_repetedCount++;
            }
            else{
                rank++;
                q_rate_infos.get(i - 1).setRank_repetedCount(rank_repetedCount);
                rank_repetedCount = 1;
            }
            q_rate_infos.get(i).setRank(rank);
        }
        q_rate_infos.get(q_rate_infos.size() - 1).setRank_repetedCount(rank_repetedCount);
    }

    private  String[] get_q_id_by_mistake_count(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_q_id_by_mistake_count(), null);

        String[] q_id_data = new String[c.getCount()];

        int i = 0;
        while (c.moveToNext()) {
            q_id_data[i] = new String();
            q_id_data[i] = c.getString(c.getColumnIndex(AiteaContract.QUESTION_INFO.Q_ID));
            i++;
        }

        c.close();
        db.close();

        return q_id_data;
    }

    private ArrayList<q_rate_info> get_all_genre_correct_rate(){

        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        ArrayList<q_rate_info> q_rate_infos = new ArrayList<>();

        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_all_genre_correct_rate(), null);

        while (c.moveToNext()) {
            q_rate_info qRateInfo = new q_rate_info();
            qRateInfo.setField_code(c.getInt(c.getColumnIndex(AiteaContract.GENRE_INFO.FIELD_CODE)));
            qRateInfo.setL_class_code(c.getInt(c.getColumnIndex(AiteaContract.GENRE_INFO.L_CLASS_CODE)));
            qRateInfo.setM_class_code(c.getInt(c.getColumnIndex(AiteaContract.GENRE_INFO.M_CLASS_CODE)));
            qRateInfo.setS_class_code(c.getInt(c.getColumnIndex(AiteaContract.GENRE_INFO.S_CLASS_CODE)));
            qRateInfo.setCorrect_rate(c.getDouble(c.getColumnIndex(AiteaContract.GENRE_INFO.S_CLASS_CORRECT_RATE)));
            q_rate_infos.add(qRateInfo);
        }

        c.close();
        db.close();

        return q_rate_infos;
    }

    private void send_to_server(){
        /* Pingによる疎通確認 */
        if(ping()) {
            /* サーバへ送信するデータを取得する */
            server_send_data[] data = get_send_data();
            int len = data.length;
            if (len > 0) {
                /* dataをdataListに詰める */
                for (int i = 0; i < len; i++) {
                    dataList.add(data[i]);
                }

                /* JSON形式に変換しjsonListに格納する */
                List<NameValuePair> jsonList = setJson("input", dataList);

                /* 送信プロセスの生成 */
                Uri.Builder builder = new Uri.Builder();

                /* DataSenderタスクを生成し、jsonListを渡す */
                DataSender sender = new DataSender(Activity_Main.this, jsonList);

                /* 送信プロセスの実行 */
                sender.execute(builder);

                /* 送信が成功したと判断した場合、サーバ送信情報テーブルの全レコードを削除 */
                delete_server_send_info();

            } else {
                Toast.makeText(this,"サーバ送信情報テーブルにデータがありません",Toast.LENGTH_LONG).show();
            }
        }else{
            Toast.makeText(this,"ネットワークに接続できません",Toast.LENGTH_LONG).show();
        }
    }

    /* DBからサーバへ送信するデータを取得するメソッド */
    private server_send_data[] get_send_data(){

        /* DatabaseとOpenHelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        /* Cursor(sql文実行結果を取得するもの)を生成し、sql文実行 */
        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_server_send_info(), null);

        /* データ保持用の配列を生成 */
        server_send_data[] data = new server_send_data[c.getCount()];

        /* sqlで実行した結果取得したテーブルを1行ずつ参照(今回は1行のみ) */
        int i=0;
        while (c.moveToNext()){
            data[i] = new server_send_data();

            /* データをテーブルから取得 */
            data[i].setExam_category(c.getString(c.getColumnIndex(AiteaContract.POST_DATA.EXAM_CATEGORY)));
            data[i].setQuestion_id(c.getString(c.getColumnIndex(AiteaContract.POST_DATA.Q_ID)));
            data[i].setUser_answer(c.getString(c.getColumnIndex(AiteaContract.POST_DATA.USER_ANSWER)));
            data[i].setCorrect_mistake(c.getString(c.getColumnIndex(AiteaContract.POST_DATA.CORRECT_MISTAKE)));
            i++;
        }
        c.close();
        db.close();
        return data;
    }


    /* 受け取ったArrayListをJSON形式に変換するメソッド */
    public List<NameValuePair> setJson (String objName, ArrayList<server_send_data> valueList) {
        /* データ保持用の配列を生成 */
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        try {
            /* JSONデータのフィールド名と要素を詰める */
            JSONArray array = new JSONArray();
            for(int i=0; i<valueList.size(); i++){
                JSONObject object = new JSONObject();
                object.put(AiteaContract.POST_DATA.EXAM_CATEGORY, valueList.get(i).getExam_category());
                object.put(AiteaContract.POST_DATA.Q_ID, valueList.get(i).getQuestion_id());
                object.put(AiteaContract.POST_DATA.USER_ANSWER, valueList.get(i).getUser_answer());
                object.put(AiteaContract.POST_DATA.CORRECT_MISTAKE, valueList.get(i).getCorrect_mistake());
                array.put(object);
            }

            /* paramsに格納する */
            BasicNameValuePair params_value = new BasicNameValuePair(objName, array.toString());
            params.add(params_value);
            return params;

        } catch (Exception e) {

            Log.i("json", "error");
            return null;
        }
    }

    /* サーバ送信情報テーブルの全レコードを削除するメソッド */
    private void delete_server_send_info(){

        /* DatabaseとOpenHelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

            /* sql文実行 */
        db.delete(AiteaContract.SERVER_SEND_INFO.TABLE_NAME, null, null);
        db.close();
    }

    /* Ping送信メソッド */
    public static boolean ping(){
        Runtime runtime = Runtime.getRuntime();
        Process process = null;
        try{
            process = runtime.exec("ping -c 1 -w 1 " + DataSender.SERVERADRESS);
            process.waitFor();
        }catch(Exception e){}
        int exitVal = process.exitValue();
        if(exitVal == 0)return true;
        else return false;
    }
}
