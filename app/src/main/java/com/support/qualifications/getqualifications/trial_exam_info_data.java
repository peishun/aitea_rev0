package com.support.qualifications.getqualifications;

import java.io.Serializable;

/**
 * tbl_trial_exam_infoのデータ型クラス
 */
public class trial_exam_info_data implements Serializable{

	private String set_q_id;
	private String q_id;
	private String user_answer;
	private int correct_mistake;
	private String user_answer_date;
	private int elapsed_time;

	public String getSet_q_id() {
		return set_q_id;
	}

	public void setSet_q_id(String set_q_id) {

		this.set_q_id = set_q_id;
	}

	public String getQ_id() {
		return q_id;
	}

	public void setQ_id(String q_id) {
		this.q_id = q_id;
	}

	public String getUser_answer() {
		return user_answer;
	}

	public void setUser_answer(String user_answer) {
		this.user_answer = user_answer;
	}

	public int getCorrect_mistake() {
		return correct_mistake;
	}

	public void setCorrect_mistake(int correct_mistake) {
		this.correct_mistake = correct_mistake;
	}

	public String getUser_answer_date() {
		return user_answer_date;
	}

	public void setUser_answer_date(String user_answer_date) {
		this.user_answer_date = user_answer_date;
	}

	public int getElapsed_time() {
		return elapsed_time;
	}

	public void setElapsed_time(int elapsed_time) {
		this.elapsed_time = elapsed_time;
	}

}
