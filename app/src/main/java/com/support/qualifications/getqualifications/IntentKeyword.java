package com.support.qualifications.getqualifications;

/**
 * Created by joho on 2015/12/21.
 */
public  final class IntentKeyword {
    public final static String Q_ID_LIST = "com.support.qualifications.getqualifications.Q_ID_LIST";
    public final static String SELECT_Q_ID = "com.support.qualifications.getqualifications.SELECT_Q_ID";
    public final static String MODE_ID = "com.support.qualifications.getqualifications.MODE_ID";
    public final static String SELECT_ANSWER_LIST = "com.support.qualifications.getqualifications.SELECT_ANSWER_LIST";
}
