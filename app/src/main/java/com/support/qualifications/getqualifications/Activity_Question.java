package com.support.qualifications.getqualifications;

import android.app.Activity;
import android.content.Context;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.text.Layout;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Activity_Question extends Activity {

    public final static String SELECT_ANSWER = "com.support.qualifications.getqualifications.SELECT_ANSWER";

    private int mode_id;
    private int currentId;
    private long startTime;

    private String[] q_id_data;
    private String selectAnswer;

    private TextView question_sentenceText1;
    private TextView question_sentenceText2;
    private ImageView question_Image;
    private TextView selection_sentenceText;
    private ImageView selection_Image;
    private Toolbar toolbar;

    private Button button_A;
    private Button button_I;
    private Button button_U;
    private Button button_E;
    private Button button_Q;

    // フラグの初期化
    public int flag_favorite = 0;
    public int flag_overcome = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

		/* xmlからViewを取得 */
        toolbar = (Toolbar) findViewById(R.id.toolbar_question);
        button_A = (Button)findViewById(R.id.Q_answerA);
        button_I = (Button)findViewById(R.id.Q_answerI);
        button_U = (Button)findViewById(R.id.Q_answerU);
        button_E = (Button)findViewById(R.id.Q_answerE);
        button_Q = (Button)findViewById(R.id.Q_answerQ);
        TextView question_sentenceText1 = (TextView) findViewById(R.id.Q_question_sentence_1);
        TextView question_sentenceText2 = (TextView) findViewById(R.id.Q_question_sentence_2);
        ImageView question_Image = (ImageView) findViewById(R.id.Q_question_image);
        LinearLayout selection_a = (LinearLayout) findViewById(R.id.Q_selection_a);
        LinearLayout selection_i = (LinearLayout) findViewById(R.id.Q_selection_i);
        LinearLayout selection_u = (LinearLayout) findViewById(R.id.Q_selection_u);
        LinearLayout selection_e = (LinearLayout) findViewById(R.id.Q_selection_e);
        TextView selection_sentenceText_a = (TextView) findViewById(R.id.Q_selection_sentence_a);
        TextView selection_sentenceText_i = (TextView) findViewById(R.id.Q_selection_sentence_i);
        TextView selection_sentenceText_u = (TextView) findViewById(R.id.Q_selection_sentence_u);
        TextView selection_sentenceText_e = (TextView) findViewById(R.id.Q_selection_sentence_e);
        ImageView selection_Image = (ImageView) findViewById(R.id.Q_selection_image);

        /* 前画面からIDと問題情報を取得する */
        Intent intent = getIntent();
        currentId = intent.getIntExtra(IntentKeyword.SELECT_Q_ID, 0);
        q_id_data = intent.getStringArrayExtra(IntentKeyword.Q_ID_LIST);
        mode_id = intent.getIntExtra(IntentKeyword.MODE_ID, 0);

        //DBからデータ取得し反映
        question_info_data[] q_i_data = get_show_data(q_id_data);

        toolbar.setTitle(q_i_data[currentId].getYear() + " " + q_i_data[currentId].getTurn() + " 問" + q_i_data[currentId].getQ_no());
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setSubtitle("一般正解率 " + String.valueOf(q_i_data[currentId].getQ_correct_rate()) + "%");
        toolbar.setSubtitleTextColor(Color.WHITE);
        toolbar.inflateMenu(R.menu.question);

        if(mode_id != AiteaContract.MODE.TRIALEXAM) {  // 模擬試験モード以外の場合、採点アイコンは非表示
            MenuItem Q_mark = toolbar.getMenu().findItem(R.id.Q_mark);
            Q_mark.setVisible(false);
        }

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {

                    // メニュー内の"成績表示"押下後の処理
                    case R.id.Q_favorite:

                        // 問題に紐付いてるフラグを取得
                        // 要修正：現状は仮に初期値を設定

                        if (flag_favorite == 0) {
                            // bookmarkを設定する時の処理

                            // 画像の変更処理
                            item.setIcon(R.drawable.icon_favorite_true);

                            // flagの設定
                            flag_favorite = 1;
                        } else {
                            // bookmarkを解除する時の処理

                            // 画像の変更処理
                            item.setIcon(R.drawable.icon_favorite_false);

                            // flagの設定
                            flag_favorite = 0;
                        }
                        break;

                    // メニュー内の"設定"押下後の処理
                    case R.id.Q_overcome:

                        // 問題に紐付いてるフラグを取得
                        // 要修正：現状は仮に初期値を設定

                        if (flag_overcome == 0) {
                            // master(克服)を設定する時の処理

                            // 画像の変更処理
                            item.setIcon(R.drawable.icon_overcome_true);

                            // flagの設定
                            flag_overcome = 1;
                        } else {
                            // master(克服)を解除する時の処理

                            // 画像の変更処理
                            item.setIcon(R.drawable.icon_overcome_false);

                            // flagの設定
                            flag_overcome = 0;
                        }
                        break;

                    // "採点"ボタン
                    // ボタン押下後、Activity_Question_Resultに遷移
                    case R.id.Q_mark:
                        goActivity_Question_Result();
                        break;
                }
                return true;
            }
        });



        /* 問題文区切り処理 */
        final String delimiter = "\\[\\[\\[img\\]\\]\\]";
        try {
            String[] Q_sentence = q_i_data[currentId].getQ_sentence().split(delimiter, -1); // delimiterで区切って配列に格納

        /* 問題文中にdelimiterが無い場合(Text-Only) */
            if (Q_sentence.length == 1) {
                question_sentenceText1.setText(Html.fromHtml(Q_sentence[0])); // sentenceText1に前文字列をセット
                question_sentenceText2.setVisibility(View.GONE); // sentenceText2にnullをセット
            } else {
            /* 前文字列が""の場合(Image-Text) */
                if (Q_sentence[0].equals("") && !Q_sentence[1].equals("")) {
                    question_sentenceText1.setVisibility(View.GONE);
                    question_sentenceText2.setText(Html.fromHtml(Q_sentence[1]));
                }
            /* 後文字列が""の場合(Text-Image) */
                else if (!Q_sentence[0].equals("") && Q_sentence[1].equals("")) {
                    question_sentenceText1.setText(Html.fromHtml(Q_sentence[0]));
                    question_sentenceText2.setVisibility(View.GONE);
                }
            /* どちらも文字列が存在し無い場合(Image-Only) */
                else if (Q_sentence[0].equals("") && Q_sentence[1].equals("")) {
                    question_sentenceText1.setVisibility(View.GONE);
                    question_sentenceText2.setVisibility(View.GONE);
                }
            /* どちらも文字列が存在する場合(Text-Image-Text) */
                else if (!Q_sentence[0].equals("") && !Q_sentence[1].equals("")) {
                    question_sentenceText1.setText(Html.fromHtml(Q_sentence[0]));
                    question_sentenceText2.setText(Html.fromHtml(Q_sentence[1]));
                }
                question_Image.setImageBitmap(q_i_data[currentId].getQ_image());
            }
        }catch (NullPointerException e){

        }

        /* 選択肢文がImage-Onlyの場合 */
        try {
            if (q_i_data[currentId].getSelection_sentence_a().equals("[[[img]]]")
                    && q_i_data[currentId].getSelection_sentence_i().equals("[[[img]]]")
                    && q_i_data[currentId].getSelection_sentence_u().equals("[[[img]]]")
                    && q_i_data[currentId].getSelection_sentence_e().equals("[[[img]]]")
                    ) {
                selection_a.setVisibility(View.GONE);
                selection_i.setVisibility(View.GONE);
                selection_u.setVisibility(View.GONE);
                selection_e.setVisibility(View.GONE);
                selection_Image.setImageBitmap(q_i_data[currentId].getSelection_image());
            }
        /* 選択肢文がText-Onlyの場合 */

            else {
                selection_sentenceText_a.setText(Html.fromHtml(q_i_data[currentId].getSelection_sentence_a()));
                selection_sentenceText_i.setText(Html.fromHtml(q_i_data[currentId].getSelection_sentence_i()));
                selection_sentenceText_u.setText(Html.fromHtml(q_i_data[currentId].getSelection_sentence_u()));
                selection_sentenceText_e.setText(Html.fromHtml(q_i_data[currentId].getSelection_sentence_e()));
            }
        }catch(NullPointerException e) {
        }


    }

    @Override
    protected void onResume() {
        super.onResume();

        ///////////////////
        // toolbarの表示 //
        ///////////////////

        String get_past_answer = get_trial_exam_user_answer(currentId + 1);

        if(mode_id == AiteaContract.MODE.TRIALEXAM) {
            int setColor = getResources().getColor(R.color.Red);

            if(get_past_answer != null){
                if (get_past_answer.equals(getString(R.string.question_answer_A))) {
                    button_A.setTextColor(setColor);
                } else if (get_past_answer.equals(getString(R.string.question_answer_I))) {
                    button_I.setTextColor(setColor);
                } else if (get_past_answer.equals(getString(R.string.question_answer_U))) {
                    button_U.setTextColor(setColor);
                } else if (get_past_answer.equals(getString(R.string.question_answer_E))) {
                    button_E.setTextColor(setColor);
                } else if (get_past_answer.equals(getString(R.string.question_answer_Q))) {
                    button_Q.setTextColor(setColor);
                }
            }
        }

        ////////////////////////////
        // ボタン押下後、画面遷移 //
        ////////////////////////////

        // "ア"ボタン
        // ボタン押下後、Activity_Question_CorrectAnswerに遷移
        button_A.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectAnswer = getString(R.string.question_answer_A);
                goNext_Activity();

            }
        });
        // "イ"ボタン
        // ボタン押下後、Activity_Question_CorrectAnswerに遷移
        button_I.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectAnswer = getString(R.string.question_answer_I);
                goNext_Activity();
            }
        });
        // "ウ"ボタン
        // ボタン押下後、Activity_Question_CorrectAnswerに遷移
        button_U.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectAnswer = getString(R.string.question_answer_U);
                goNext_Activity();
            }
        });
        // "エ"ボタン
        // ボタン押下後、Activity_Question_CorrectAnswerに遷移
        button_E.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectAnswer = getString(R.string.question_answer_E);
                goNext_Activity();
            }
        });
        // "？"ボタン
        // ボタン押下後、Activity_Question_CorrectAnswerに遷移
        button_Q.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectAnswer = getString(R.string.question_answer_Q);
                goNext_Activity();
            }
        });

        startTime = SystemClock.elapsedRealtime();
    }

    private void goNext_Activity(){
        if( mode_id == AiteaContract.MODE.TRIALEXAM ) {
            update_trial_exam_db();
            go_Next_Activity_Question();
        }else{
            goActivity_Question_CorrectAnswer();
        }
    }
    private void goActivity_Question_Result(){
        // オブジェクト生成
        Intent intent = new Intent(Activity_Question.this, Activity_Question_Result.class);

        Activity_Mode_TrialExam_List.Fflag.setFlag(true);

        // intentの実行
        startActivity(intent);

        finish();
    }
    private question_info_data[] get_show_data(String[] q_id_data) {

        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        question_info_data[] out_q_i_data = new question_info_data[q_id_data.length];

        for (int i = 0; i < q_id_data.length; i++) {
            Cursor c = null;
            c = db.rawQuery(DB_makesql.get_q_info(q_id_data[i]), null);

            /* sqlで実行した結果取得したテーブルを1行ずつ参照(今回は1行のみ) */
            while (c.moveToNext()) {
                /* データをテーブルから取得 */

                out_q_i_data[i] = new question_info_data();
                out_q_i_data[i].setYear(c.getString(c.getColumnIndex(AiteaContract.QUESTION_INFO.YEAR)));
                out_q_i_data[i].setTurn(c.getString(c.getColumnIndex(AiteaContract.QUESTION_INFO.TURN)));
                out_q_i_data[i].setQ_no(c.getInt(c.getColumnIndex(AiteaContract.QUESTION_INFO.Q_NO)));
                out_q_i_data[i].setQ_name(c.getString(c.getColumnIndex(AiteaContract.QUESTION_INFO.Q_NAME)));
                out_q_i_data[i].setQ_sentence(c.getString(c.getColumnIndex(AiteaContract.QUESTION_INFO.Q_SENTENCE)));
                out_q_i_data[i].setQ_image(c.getBlob(c.getColumnIndex(AiteaContract.QUESTION_INFO.Q_IMAGE)));
                out_q_i_data[i].setSelection_sentence_a(c.getString(c.getColumnIndex(AiteaContract.QUESTION_INFO.SELECTION_SENTENCE_A)));
                out_q_i_data[i].setSelection_sentence_i(c.getString(c.getColumnIndex(AiteaContract.QUESTION_INFO.SELECTION_SENTENCE_I)));
                out_q_i_data[i].setSelection_sentence_u(c.getString(c.getColumnIndex(AiteaContract.QUESTION_INFO.SELECTION_SENTENCE_U)));
                out_q_i_data[i].setSelection_sentence_e(c.getString(c.getColumnIndex(AiteaContract.QUESTION_INFO.SELECTION_SENTENCE_E)));
                out_q_i_data[i].setSelection_image(c.getBlob(c.getColumnIndex(AiteaContract.QUESTION_INFO.SELECTION_IMAGE)));
                out_q_i_data[i].setQ_correct_rate(c.getDouble(c.getColumnIndex(AiteaContract.QUESTION_INFO.Q_CORRECT_RATE)));
                out_q_i_data[i].setFavorite_flag(c.getInt(c.getColumnIndex(AiteaContract.QUESTION_INFO.FAVORITE_FLAG)));
                out_q_i_data[i].setOvercome_flag(c.getInt(c.getColumnIndex(AiteaContract.QUESTION_INFO.OVERCOME_FLAG)));
            }
            c.close();
        }
        db.close();

        return out_q_i_data;
    }

    private void update_trial_exam_db(){

        long elapsed_time_calc =  ( SystemClock.elapsedRealtime() - startTime ) / 1000 ;

        int elapsed_time = (int)elapsed_time_calc;

        String nowDate = AiteaContract.getNowDate();

        int correct_mistake = 0;
        if( selectAnswer.equals(get_correctAnswer(q_id_data[currentId])) ){
            correct_mistake = 1;
        }
        update_trial_exam_info(currentId + 1, selectAnswer, correct_mistake, nowDate, elapsed_time);
    }



    private void go_Next_Activity_Question() {

        int nextId = currentId + 1;

        if (nextId == q_id_data.length || isExistAnswerRecode()) {

        }
        else{
            Intent intent = new Intent(Activity_Question.this, Activity_Question.class);
            intent.putExtra(IntentKeyword.Q_ID_LIST, q_id_data);
            intent.putExtra(IntentKeyword.SELECT_Q_ID, nextId);
            intent.putExtra(IntentKeyword.MODE_ID, mode_id);
            // intentの実行
            startActivity(intent);
        }
        finish();
    }

    private boolean isExistAnswerRecode(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_trial_exam_already_answer_recode(), null);

        while (c.moveToNext()) {
            if( c.getString(c.getColumnIndex(AiteaContract.TRIAL_EXAM_INFO.USER_ANSWER)) == null ) {
                c.close();
                db.close();
                return false;
            }
        }

        c.close();
        db.close();

        return true;
    }

    private void goActivity_Question_CorrectAnswer() {

        Intent intent = new Intent(Activity_Question.this, Activity_Question_CorrectAnswer.class);
        intent.putExtra(IntentKeyword.Q_ID_LIST, q_id_data);
        intent.putExtra(IntentKeyword.SELECT_Q_ID, currentId);
        intent.putExtra(SELECT_ANSWER, selectAnswer);
        intent.putExtra(IntentKeyword.MODE_ID, mode_id);

        // intentの実行
        startActivity(intent);

        finish();
    }

    private void update_trial_exam_info(int set_q_id,String q_id ,int correct_mistake,String user_answer_date,int elapsed_time){
        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        db.execSQL(DB_makesql.update_trial_exam_info(set_q_id, q_id, correct_mistake, user_answer_date,elapsed_time));

        db.close();

    }

    private String get_correctAnswer(String q_id){

        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_correctAnswer(q_id), null);

            /* sqlで実行した結果取得したテーブルを1行ずつ参照(今回は1行のみ) */
        if (c.moveToNext()) {
            return c.getString(c.getColumnIndex(AiteaContract.QUESTION_INFO.CORRECT_ANSWER));
        }
        c.close();
        db.close();
        return null;
    }


    private String get_trial_exam_user_answer( int set_q_id){
        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_trial_exam_user_answer(set_q_id), null);

            /* sqlで実行した結果取得したテーブルを1行ずつ参照(今回は1行のみ) */
        if (c.moveToNext()) {
            return c.getString(c.getColumnIndex(AiteaContract.TRIAL_EXAM_INFO.USER_ANSWER));
        }
        c.close();
        db.close();
        return null;
    }
}
