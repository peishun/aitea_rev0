package com.support.qualifications.getqualifications;

import android.app.Activity;
import android.net.Uri;
import android.os.AsyncTask;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.util.List;

public class DataSender extends AsyncTask<Uri.Builder, Void, String> {

    public static final String SERVERADRESS = "192.168.1.38";
    private Activity mainActivity;
    private List<NameValuePair> list;

    public DataSender(Activity activity, List<NameValuePair> list) {
        this.mainActivity = activity; // 呼び出し元のアクティビティ
        this.list = list;
    }

    /* 非同期で処理されるらしいメソッド */
    @Override
    protected String doInBackground(Uri.Builder... builder) {

            /* HttpPost通信の宛先ホストを指定 */
        HttpPost post = new HttpPost("http://" + SERVERADRESS + ":8080/Aitea-Manager-Inter/postdata");

            /* HttpClientを生成 */
        HttpClient client = new DefaultHttpClient();

            /* レスポンス格納用変数 */
        HttpResponse httpResponse = null;

        try {
                /* HttpPostに送信するデータをセットする */
            post.setEntity(new UrlEncodedFormEntity(list,"UTF-8"));

                /* Postを実行し、HttpResponseを取得する */
            httpResponse = client.execute(post);

                /* ステータスコードを取得する */
            int statusCode = httpResponse.getStatusLine().getStatusCode();
            return "送信が成功しました！(コード：" + statusCode + ")" ;

        } catch (Exception e) {
            e.printStackTrace();
            return "送信が失敗しました..." ;
        }
    }

    /* 非同期処理終了後に呼び出されるメソッド */
    @Override
    protected void onPostExecute(String result) {
            /* 取得した結果を表示したりする */

    }

}
