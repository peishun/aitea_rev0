package com.support.qualifications.getqualifications;

import java.util.ArrayList;
import java.util.List;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;


public class Activity_Mode_Dictionary_Genre extends Activity {

    private int mode_id;

    private static final String NULL_ITEM = "―";

    private List<String> listItem;
    private List<String> listNull;
    private ArrayAdapter<String> null_adapter;
    private genre_info_data[] field_i_data;
    private genre_info_data[] lclass_i_data;
    private genre_info_data[] mclass_i_data;
    private genre_info_data[] sclass_i_data;
    private int select_sclass_id;
    private Spinner spinner_filed;
    private Spinner spinner_class_large;
    private Spinner spinner_class_middle;
    private Spinner spinner_class_small;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mode_dictionary_genre);

         /* 前画面からMODE_IDを取得する */
        Intent intent = getIntent();
        mode_id = intent.getIntExtra(IntentKeyword.MODE_ID, 0);

        listNull = new ArrayList<String>();
        listNull.add(NULL_ITEM);
        null_adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, listNull);

        field_i_data = get_field_info();
        listItem = mkField_name_list(field_i_data);
        ArrayAdapter<String> spinAdapter_field = new ArrayAdapter<String>(Activity_Mode_Dictionary_Genre.this, android.R.layout.simple_list_item_1, listItem);
        lclass_i_data = get_Lclass_info(field_i_data[0].getField_code());


        listItem = mkL_class_name_list(lclass_i_data);
        final ArrayAdapter<String> spinAdapter_class_large = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listItem);

        // ====================
        // Spinnerの処理
        // ====================

        // spinner_filed(分野)の処理
        spinner_filed = (Spinner) findViewById(R.id.spinner_filed);
        // SpinnerにAdapterを設定
        spinner_filed.setAdapter(spinAdapter_field);

        // spinner_class_large(大分類)の処理
        spinner_class_large = (Spinner) findViewById(R.id.spinner_class_large);
        // SpinnerにAdapterを設定
        spinner_class_large.setAdapter(spinAdapter_class_large);

        // spinner_class_middle(中分類)の処理
        spinner_class_middle = (Spinner) findViewById(R.id.spinner_class_middle);
        // SpinnerにAdapterを設定
        spinner_class_middle.setAdapter(null_adapter);

        // spinner_class_small(小分類)の処理
        spinner_class_small = (Spinner) findViewById(R.id.spinner_class_small);
        // SpinnerにAdapterを設定
        spinner_class_small.setAdapter(null_adapter);

        // Spinnerのアイテムを選択時に行う処理
        spinner_filed.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            // アイテムが選択された場合
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                lclass_i_data = get_Lclass_info(field_i_data[(int) id].getField_code());
                listItem = mkL_class_name_list(lclass_i_data);
                ArrayAdapter<String> spinAdapter_class = new ArrayAdapter<String>(Activity_Mode_Dictionary_Genre.this, android.R.layout.simple_list_item_1, listItem);
                spinner_class_large.setAdapter(spinAdapter_class);
                mclass_i_data = null;
                spinner_class_middle.setAdapter(null_adapter);
                spinner_class_middle.setEnabled(false);
                sclass_i_data = null;
                spinner_class_small.setAdapter(null_adapter);
                spinner_class_small.setEnabled(false);
            }

            // 何も選択されなかった場合
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        // Spinnerのアイテムを選択時に行う処理
        spinner_class_large.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            // アイテムが選択された場合
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                sclass_i_data = null;
                spinner_class_small.setAdapter(null_adapter);
                spinner_class_small.setEnabled(false);

                if( id == 0 ) {
                    mclass_i_data = null;
                    spinner_class_middle.setAdapter(null_adapter);
                    spinner_class_middle.setEnabled(false);
                }
                else{
                    mclass_i_data = get_Mclass_info(lclass_i_data[(int) id - 1].getField_code(), lclass_i_data[(int)id - 1].getL_class_code());
                    listItem = mkM_class_name_list(mclass_i_data);
                    ArrayAdapter<String> spinAdapter_class = new ArrayAdapter<String>(Activity_Mode_Dictionary_Genre.this, android.R.layout.simple_list_item_1, listItem);
                    spinner_class_middle.setAdapter(spinAdapter_class);
                    spinner_class_middle.setEnabled(true);
                }
            }

            // 何も選択されなかった場合
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        // Spinnerのアイテムを選択時に行う処理
        spinner_class_middle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            // アイテムが選択された場合
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if( id == 0 ) {
                    sclass_i_data = null;
                    spinner_class_small.setAdapter(null_adapter);
                    spinner_class_small.setEnabled(false);
                }
                else{
                    sclass_i_data = get_Sclass_info(
                            mclass_i_data[(int)id - 1].getField_code() ,
                            mclass_i_data[(int)id - 1].getL_class_code(),
                            mclass_i_data[(int)id - 1].getM_class_code()
                    );
                    listItem = mkS_class_name_list(sclass_i_data);
                    ArrayAdapter<String> spinAdapter_class = new ArrayAdapter<String>(Activity_Mode_Dictionary_Genre.this, android.R.layout.simple_list_item_1, listItem);
                    spinner_class_small.setAdapter(spinAdapter_class);
                    spinner_class_small.setEnabled(true);
                }
            }

            // 何も選択されなかった場合
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        // Spinnerのアイテムを選択時に行う処理
        spinner_class_small.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            // アイテムが選択された場合
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    select_sclass_id = (int)id - 1;
            }

            // 何も選択されなかった場合
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
        // ====================
        // Buttonの処理
        // ====================

        // 検索ボタン
        findViewById(R.id.button_mode_dictionary_genre_list).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // オブジェクト生成及びintentに遷移先のパッケージ名とアクティビティ名を引数に設定
                Intent intent = new Intent(Activity_Mode_Dictionary_Genre.this,Activity_Mode_Dictionary_Question_List.class);

                String[] out_q_id_data = null;

                if( select_sclass_id != -1 ){
                    out_q_id_data = getOut_q_i_data(
                            sclass_i_data[0].getField_code(),
                            sclass_i_data[0].getL_class_code(),
                            sclass_i_data[0].getM_class_code(),
                            sclass_i_data[select_sclass_id].getS_class_code()
                    );

                }
                else if( sclass_i_data != null ){
                    out_q_id_data = getOut_q_i_data(
                            sclass_i_data[0].getField_code(),
                            sclass_i_data[0].getL_class_code(),
                            sclass_i_data[0].getM_class_code()
                    );
                }
                else if( mclass_i_data != null ){
                    out_q_id_data = getOut_q_i_data(
                            mclass_i_data[0].getField_code(),
                            mclass_i_data[0].getL_class_code()
                            );
                }
                else{
                    out_q_id_data = getOut_q_i_data(lclass_i_data[0].getField_code());
                }

                //遷移先に渡す値
                intent.putExtra(IntentKeyword.Q_ID_LIST, out_q_id_data);
                intent.putExtra(IntentKeyword.MODE_ID, mode_id);

                // intentの実行
                startActivity(intent);
            }
        });
        ///////////////////
        // toolbarの表示 //
        ///////////////////

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_mode_dictionary_genre);
        toolbar.setTitle("ジャンル検索");
        toolbar.setTitleTextColor(Color.WHITE);
    }


    private genre_info_data[] get_field_info(){

        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        /* Cursor(sql文実行結果を取得するもの)を生成し、sql文実行 */
        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_all_field_info(), null);

        /* 行数文の大きさのデータクラス型配列 */
        genre_info_data[] out_g_i_data = new genre_info_data[c.getCount()];

        int i = 0;
        /* sqlで実行した結果取得したテーブルを1行ずつ参照 */
        while (c.moveToNext()){
            out_g_i_data[i] = new genre_info_data();
            out_g_i_data[i].setField_code(c.getInt(c.getColumnIndex(AiteaContract.GENRE_INFO.FIELD_CODE)));
            out_g_i_data[i].setField_name(c.getString(c.getColumnIndex(AiteaContract.GENRE_INFO.FIELD_NAME)));
            i++;
        }

        c.close();
        db.close();

        return out_g_i_data;
    }

    private genre_info_data[] get_Lclass_info(int field_code){

        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        /* Cursor(sql文実行結果を取得するもの)を生成し、sql文実行 */
        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_l_class_info(field_code), null);

        /* 行数文の大きさのデータクラス型配列 */
        genre_info_data[] out_g_i_data = new genre_info_data[c.getCount()];

        int i = 0;
        /* sqlで実行した結果取得したテーブルを1行ずつ参照 */
        while (c.moveToNext()){
            out_g_i_data[i] = new genre_info_data();
            out_g_i_data[i].setField_code(field_code);
            out_g_i_data[i].setL_class_code(c.getInt(c.getColumnIndex(AiteaContract.GENRE_INFO.L_CLASS_CODE)));
            out_g_i_data[i].setL_class_name(c.getString(c.getColumnIndex(AiteaContract.GENRE_INFO.L_CLASS_NAME)));
            i++;
        }

        c.close();
        db.close();

        return out_g_i_data;
    }


    private genre_info_data[] get_Mclass_info(int field_code,int l_class_code){

        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        /* Cursor(sql文実行結果を取得するもの)を生成し、sql文実行 */
        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_m_class_info(field_code, l_class_code), null);

        /* 行数文の大きさのデータクラス型配列 */
        genre_info_data[] out_g_i_data = new genre_info_data[c.getCount()];

        int i = 0;
        /* sqlで実行した結果取得したテーブルを1行ずつ参照 */
        while (c.moveToNext()){
            out_g_i_data[i] = new genre_info_data();
            out_g_i_data[i].setField_code(field_code);
            out_g_i_data[i].setL_class_code(l_class_code);
            out_g_i_data[i].setM_class_code(c.getInt(c.getColumnIndex(AiteaContract.GENRE_INFO.M_CLASS_CODE)));
            out_g_i_data[i].setM_class_name(c.getString(c.getColumnIndex(AiteaContract.GENRE_INFO.M_CLASS_NAME)));
            i++;
        }

        c.close();
        db.close();

        return out_g_i_data;
    }


    private genre_info_data[] get_Sclass_info(int field_code,int l_class_code,int m_class_code){

        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        /* Cursor(sql文実行結果を取得するもの)を生成し、sql文実行 */
        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_s_class_info(field_code, l_class_code, m_class_code), null);

        /* 行数文の大きさのデータクラス型配列 */
        genre_info_data[] out_g_i_data = new genre_info_data[c.getCount()];

        int i = 0;
        /* sqlで実行した結果取得したテーブルを1行ずつ参照 */
        while (c.moveToNext()) {
            out_g_i_data[i] = new genre_info_data();
            out_g_i_data[i].setField_code(field_code);
            out_g_i_data[i].setL_class_code(l_class_code);
            out_g_i_data[i].setM_class_code(m_class_code);
            out_g_i_data[i].setS_class_code(c.getInt(c.getColumnIndex(AiteaContract.GENRE_INFO.S_CLASS_CODE)));
            out_g_i_data[i].setS_class_name(c.getString(c.getColumnIndex(AiteaContract.GENRE_INFO.S_CLASS_NAME)));
            i++;
        };

        c.close();
        db.close();

        return out_g_i_data;
    }


    private String[] getOut_q_i_data(int field_code){

        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        /* Cursor(sql文実行結果を取得するもの)を生成し、sql文実行 */
        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_q_id(field_code), null);

        if(c.getCount() == 0){
            return null;
        }

        /* 行数文の大きさのデータクラス型配列 */
        String[] out_q_i_data = new String[c.getCount()];

        int i = 0;
        /* sqlで実行した結果取得したテーブルを1行ずつ参照 */
        while (c.moveToNext()){
            out_q_i_data[i] = new String();
            out_q_i_data[i] = c.getString(c.getColumnIndex(AiteaContract.QUESTION_INFO.Q_ID));
            i++;
        }


        c.close();
        db.close();

        return out_q_i_data;
    }


    private String[] getOut_q_i_data(int field_code, int l_class_code){

        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        /* Cursor(sql文実行結果を取得するもの)を生成し、sql文実行 */
        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_q_id(field_code, l_class_code), null);

        if(c.getCount() == 0){
            return null;
        }

        /* 行数文の大きさのデータクラス型配列 */
        String[] out_q_i_data = new String[c.getCount()];

        int i = 0;
        /* sqlで実行した結果取得したテーブルを1行ずつ参照 */
        while (c.moveToNext()){
            out_q_i_data[i] = new String();
            out_q_i_data[i] = c.getString(c.getColumnIndex(AiteaContract.QUESTION_INFO.Q_ID));
            i++;
        }


        c.close();
        db.close();

        return out_q_i_data;
    }


    private String[] getOut_q_i_data(int field_code, int l_class_code , int m_class_code){

        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        /* Cursor(sql文実行結果を取得するもの)を生成し、sql文実行 */
        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_q_id(field_code, l_class_code, m_class_code), null);

        if(c.getCount() == 0){
            return null;
        }

        /* 行数文の大きさのデータクラス型配列 */
        String[] out_q_i_data = new String[c.getCount()];

        int i = 0;
        /* sqlで実行した結果取得したテーブルを1行ずつ参照 */
        while (c.moveToNext()){
            out_q_i_data[i] = new String();
            out_q_i_data[i] = c.getString(c.getColumnIndex(AiteaContract.QUESTION_INFO.Q_ID));
            i++;
        }


        c.close();
        db.close();

        return out_q_i_data;
    }


    private String[] getOut_q_i_data(int field_code, int l_class_code , int m_class_code , int s_class_code) {

        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        /* Cursor(sql文実行結果を取得するもの)を生成し、sql文実行 */
        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_q_id(field_code, l_class_code, m_class_code, s_class_code), null);

        if(c.getCount() == 0){
            return null;
        }

        /* 行数文の大きさのデータクラス型配列 */
        String[] out_q_i_data = new String[c.getCount()];

        int i = 0;
        /* sqlで実行した結果取得したテーブルを1行ずつ参照 */
        while (c.moveToNext()){
            out_q_i_data[i] = new String();
            out_q_i_data[i] = c.getString(c.getColumnIndex(AiteaContract.QUESTION_INFO.Q_ID));
            i++;
        }

        c.close();
        db.close();

        return out_q_i_data;
    }

    /**
     * 指定したデータのfield_name_list( List<String> )を生成
     *
     * @param g_i_data 格納する文字をもつジャンルデータ配列
     * @return　文字列リスト
     */
    private List<String> mkField_name_list(genre_info_data[] g_i_data) {
        List<String> field_name_list = new ArrayList<String>();
        for (int i = 0; i < g_i_data.length; i++) {
            field_name_list.add(g_i_data[i].getField_name());
        }
        return field_name_list;
    }


    /**
     *  指定したデータ,codeのl_class_name_list( List<String> )を生成
     * @param g_i_data  格納する文字をもつジャンルデータ配列
     * @return 文字列リスト
     */
    private List<String> mkL_class_name_list(genre_info_data[] g_i_data) {
        List<String> l_class_name_list = new ArrayList<String>();
        l_class_name_list.add(NULL_ITEM);
        for (int i = 0; i < g_i_data.length; i++) {
            l_class_name_list.add(g_i_data[i].getL_class_name());
        }
        return l_class_name_list;
    }

    /**
     *  指定したデータ,codeのm_class_name_list( List<String> )を生成
     * @param g_i_data  格納する文字をもつジャンルデータ配列
     * @return 文字列リスト
     */
    private List<String> mkM_class_name_list(genre_info_data[] g_i_data) {
        List<String> m_class_name_list = new ArrayList<String>();
        m_class_name_list.add(NULL_ITEM);
        for (int i = 0; i < g_i_data.length; i++) {
            m_class_name_list.add(g_i_data[i].getM_class_name());
        }
        return m_class_name_list;
    }

    /**
     *  指定したデータ,codeのs_class_name_list( List<String> )を生成
     * @param g_i_data  格納する文字をもつジャンルデータ配列
     * @return 文字列リスト
     */
    private List<String> mkS_class_name_list(genre_info_data[] g_i_data) {
        List<String> s_class_name_list = new ArrayList<String>();
        s_class_name_list.add(NULL_ITEM);
        for (int i = 0; i < g_i_data.length; i++) {
            s_class_name_list.add(g_i_data[i].getS_class_name());
        }
        return s_class_name_list;
    }
}