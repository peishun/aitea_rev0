package com.support.qualifications.getqualifications;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class Activity_Mode_Dictionary extends Activity {

    private int mode_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mode_dictionary);

        /* 前画面からMODE_IDを取得する */
        Intent intent = getIntent();
        mode_id = intent.getIntExtra(IntentKeyword.MODE_ID, 0);

        ////////////////////////////
        // ボタン押下後、画面遷移 //
        ////////////////////////////

        // ジャンル別ボタン
        findViewById(R.id.button_mode_dictionary_genre).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // オブジェクト生成
                Intent intent = new Intent(Activity_Mode_Dictionary.this,Activity_Mode_Dictionary_Genre.class);
                // 遷移先に渡す値
                intent.putExtra(IntentKeyword.MODE_ID, mode_id);
                // intentの実行
                startActivity(intent);
            }
        });

        // 年度別ボタン
        findViewById(R.id.button_mode_dictionary_year).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // オブジェクト生成
                Intent intent = new Intent(Activity_Mode_Dictionary.this,Activity_Mode_Dictionary_Year.class);
                // 遷移先に渡す値
                intent.putExtra(IntentKeyword.MODE_ID, mode_id);
                // intentの実行
                startActivity(intent);
            }
        });

        ///////////////////
        // toolbarの表示 //
        ///////////////////

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_mode_dictionary);
        toolbar.setTitle("問題辞書");
        toolbar.setTitleTextColor(Color.WHITE);
    }
}
