// 参考URL http://ichitcltk.hustle.ne.jp/gudon2/index.php?pageType=file&id=Android028_ListView1

package com.support.qualifications.getqualifications;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;


public class Activity_Mode_Dictionary_Question_List extends Activity implements CompoundButton.OnCheckedChangeListener{

    private Toolbar toolbar;
    private ListView myListview;
    private boolean isRandomMode;

    private int mode_id;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mode_dictionary_question_list);

        ///////////////////
        // toolbarの表示 //
        ///////////////////

        toolbar = (Toolbar) findViewById(R.id.toolbar_mode_dictionary_question_list);

        //リストビューの取得
        myListview = (ListView)findViewById(R.id.listview_dictionary_question_list);

        myListview.setEmptyView(findViewById(R.id.empty));

        Switch random_switch = (Switch)findViewById(R.id.random_switch);

        random_switch.setOnCheckedChangeListener(this);

        toolbar.setTitle("ジャンル名を表示");
        toolbar.setTitleTextColor(Color.WHITE);

        //リストに表示するデータの配列
        ArrayList<User> users = new ArrayList<>();

        /* 前画面からIDと問題情報を取得する */
        Intent intent = getIntent();

        mode_id = intent.getIntExtra(IntentKeyword.MODE_ID, 0);

        final String[] in_qid_data = intent.getStringArrayExtra(IntentKeyword.Q_ID_LIST);

        if( in_qid_data != null ) {

            //DBからデータ取得し反映
            final question_info_data[] q_i_data = get_show_data(in_qid_data);

            int icons = R.mipmap.ic_launcher;

            //リストに表示するデータをセットする
            for (int i = 0; i < q_i_data.length; i++) {
                User user = new User();
                user.setIcon(BitmapFactory.decodeResource(getResources(), icons));
                user.setQ_info(q_i_data[i].getYear() + " " + q_i_data[i].getTurn() + " 問" + q_i_data[i].getQ_no());
                user.setQ_summary(q_i_data[i].getQ_name());
                users.add(user);
            }

            //リストに表示するデータをアダプターにセット
            UserAdapter adapter = new UserAdapter(this, 0, users);

            myListview.setAdapter(adapter);

            //リストをタップしたときのEvent
            myListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(
                        AdapterView<?> parent,
                        View view, //タップされたビュー
                        int position, //何番目？
                        long id //View id
                ) {

                    // オブジェクト生成及びintentに遷移先のパッケージ名とアクティビティ名を引数に設定
                    Intent intent = new Intent(Activity_Mode_Dictionary_Question_List.this, Activity_Question.class);

                    //遷移先に渡す値
                    if(isRandomMode) {
                        //配列からlistへ変換
                        ArrayList<String> list = new ArrayList<String>(Arrays.asList(in_qid_data));

                        //選択した問題のq_idを退避
                        String first_q_id = list.get(position);
                        list.remove(position);

                        //リストの並びをシャッフル
                        Collections.shuffle(list);

                        //退避したq_idを先頭に追加
                        list.add(0, first_q_id);

                        //シャッフルしたデータを返す
                        final String[] shuffle_qid_data = (String[])list.toArray(new String[list.size()]);

                        intent.putExtra(IntentKeyword.SELECT_Q_ID, 0);
                        intent.putExtra(IntentKeyword.Q_ID_LIST, shuffle_qid_data);

                    }else{
                        intent.putExtra(IntentKeyword.SELECT_Q_ID, position);
                        intent.putExtra(IntentKeyword.Q_ID_LIST, in_qid_data);
                    }

                    intent.putExtra(IntentKeyword.MODE_ID, mode_id);
                    // intentの実行
                    startActivity(intent);

                }
            });
        }

    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    private HashMap<String,question_info_data> get_all_list_q_id_info(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        HashMap<String,question_info_data> map = new HashMap<>();

        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_all_list_q_id_info(), null);

        while (c.moveToNext()) {
            // keyをつくる
            String key = c.getString(c.getColumnIndex(AiteaContract.QUESTION_INFO.Q_ID));

            question_info_data questionInfoData = new question_info_data();
            questionInfoData.setYear(c.getString(c.getColumnIndex(AiteaContract.QUESTION_INFO.YEAR)));
            questionInfoData.setTurn(c.getString(c.getColumnIndex(AiteaContract.QUESTION_INFO.TURN)));
            questionInfoData.setQ_no(c.getInt(c.getColumnIndex(AiteaContract.QUESTION_INFO.Q_NO)));
            questionInfoData.setQ_name(c.getString(c.getColumnIndex(AiteaContract.QUESTION_INFO.Q_NAME)));

            map.put(key, questionInfoData);
        }

        c.close();
        db.close();

        return map;

    }
    private question_info_data[] get_show_data(String[] q_id_data){

        HashMap<String,question_info_data> q_i_data_list = get_all_list_q_id_info();

        question_info_data[] out_q_i_data = new question_info_data[q_id_data.length];

        for( int i = 0; i < q_id_data.length; i++ ){
            out_q_i_data[i] = q_i_data_list.get(q_id_data[i]);
        }

        return out_q_i_data;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        if(isChecked == true){
            isRandomMode = true;
        }else{
            isRandomMode = false;
        }
    }

    public class UserAdapter extends ArrayAdapter<User> {

        private LayoutInflater layoutInflater;

        public UserAdapter(Context c, int id, ArrayList<User> users) {
            super(c, id, users);
            this.layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        /* リストをスクロールして、新たにリストが表示されるときに呼ばれる */
        @Override
        public View getView(int pos, View convertview, ViewGroup parent){

            ViewHolder holder;

            /* 再利用されるビューが無い場合、 */
            if( convertview == null ){
                convertview = layoutInflater.inflate(
                        R.layout.question_list_item,
                        parent,
                        false
                );
                holder = new ViewHolder();
                holder.icon = (ImageView) convertview.findViewById(R.id.questionicon);
                holder.info = (TextView) convertview.findViewById(R.id.questionInfo);
                holder.summary = (TextView) convertview.findViewById(R.id.questionsummary);
                convertview.setTag(holder);//データ保持
            }else{
                holder = (ViewHolder)convertview.getTag();
            }

            /* Userデータを取ってきて表示 */
            User user = (User)getItem(pos);

            holder.icon.setImageBitmap(user.getIcon());
            holder.info.setText(user.getQ_info());
            holder.summary.setText(user.getQ_summary());

            return convertview;

        }
    }

    //リストに表示するデータ定義
    static class ViewHolder{
        ImageView icon;
        TextView info;
        TextView summary;
    }

    //リストに表示するデータ型クラス
    private class User{
        private Bitmap icon;
        private String q_info;
        private String q_summary;

        public void setIcon(Bitmap icon) {
            this.icon = icon;
        }

        public void setQ_info(String q_info) {
            this.q_info = q_info;
        }

        public void setQ_summary(String q_summary) {
            this.q_summary = q_summary;
        }

        public Bitmap getIcon() {

            return icon;
        }

        public String getQ_info() {
            return q_info;
        }

        public String getQ_summary() {
            return q_summary;
        }
    }


}
