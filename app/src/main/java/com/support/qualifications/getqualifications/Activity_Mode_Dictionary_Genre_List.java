// 参考URL http://ichitcltk.hustle.ne.jp/gudon2/index.php?pageType=file&id=Android028_ListView1

package com.support.qualifications.getqualifications;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class Activity_Mode_Dictionary_Genre_List extends Activity {

    private int mode_id;

    public final static String SELECT_Q_ID = "com.support.qualifications.getqualifications.SELECT_Q_ID";
    public final static String Q_I_DATA = "com.support.qualifications.getqualifications.Q_I_DATA";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mode_dictionary_genre_list);


        /* 前画面からMODE_IDを取得する */
        Intent intent = getIntent();
        mode_id = intent.getIntExtra(IntentKeyword.MODE_ID, 0);

        /* テストデータ */
        final question_info_data[] q_i_data = new question_info_data[160];
        for( int i = 0 ; i < q_i_data.length; i++ ){
            q_i_data[i] = new question_info_data();
            q_i_data[i].setQ_id("Q" + String.format("%04d",i+961));
        }

        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        /* Cursor(sql文実行結果を取得するもの)を生成し、sql文実行 */
        Cursor c = null;

        for( int i = 0; i < q_i_data.length; i++ ) {
            c = db.rawQuery(DB_makesql.get_qlst_info(q_i_data[i].getQ_id()), null);

            /* sqlで実行した結果取得したテーブルを1行ずつ参照(今回は1行のみ) */
            while (c.moveToNext()) {

                /* データをテーブルから取得 */
                q_i_data[i].setQ_no(c.getInt(c.getColumnIndex(AiteaContract.QUESTION_INFO.Q_ID)));
                q_i_data[i].setYear(c.getString(c.getColumnIndex(AiteaContract.QUESTION_INFO.YEAR)));
                q_i_data[i].setTurn(c.getString(c.getColumnIndex(AiteaContract.QUESTION_INFO.TURN)));
                q_i_data[i].setQ_no(c.getInt(c.getColumnIndex(AiteaContract.QUESTION_INFO.Q_NO)));
                q_i_data[i].setQ_name(c.getString(c.getColumnIndex(AiteaContract.QUESTION_INFO.Q_NAME)));
            }
        }

        c.close();
        db.close();

        //リストビューの取得
        ListView myListview = (ListView)findViewById(R.id.listview_dictionary_genre_list);

        //リストに表示するデータの配列
        ArrayList<User> users = new ArrayList<>();

//        int icons = R.mipmap.ic_launcher;

        String[] questionInfo = new String[q_i_data.length];
        String[] questionsummary = new String[q_i_data.length];

        for( int i = 0; i < q_i_data.length; i++ ){
            questionInfo[i] = q_i_data[i].getYear() + " " + q_i_data[i].getTurn() + " 問" + q_i_data[i].getQ_no();
            questionsummary[i] = q_i_data[i].getQ_name();
        }

        //リストに表示するデータをセットする
        for( int i = 0; i < questionInfo.length ; i++){
            User user = new User();
//            user.setIcon(BitmapFactory.decodeResource(getResources(),icons));
            user.setQ_info(questionInfo[i]);
            user.setQ_summary(questionsummary[i]);
            users.add(user);
        }

        //リストに表示するデータをアダプターにセット
        UserAdapter adapter = new UserAdapter(this,0,users);

        myListview.setEmptyView(findViewById(R.id.empty));
        myListview.setAdapter(adapter);

        //リストをタップしたときのEvent
        myListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(
                    AdapterView<?> parent,
                    View view, //タップされたビュー
                    int position, //何番目？
                    long id //View id
            ) {

                // オブジェクト生成及びintentに遷移先のパッケージ名とアクティビティ名を引数に設定
                Intent intent = new Intent(Activity_Mode_Dictionary_Genre_List.this, Activity_Question.class);

                //遷移先に渡す値
                intent.putExtra(SELECT_Q_ID, position);
                intent.putExtra(Q_I_DATA,q_i_data);
                intent.putExtra(IntentKeyword.MODE_ID, mode_id);

                // intentの実行
                startActivity(intent);

            }
        });

        ///////////////////
        // toolbarの表示 //
        ///////////////////

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_mode_genre_list);
        toolbar.setTitle("分野 or ジャンル名を入れる");
        toolbar.setTitleTextColor(Color.WHITE);

    }

    public class UserAdapter extends ArrayAdapter<User> {

        private LayoutInflater layoutInflater;

        public UserAdapter(Context c, int id, ArrayList<User> users) {
            super(c, id, users);
            this.layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        /* リストをスクロールして、新たにリストが表示されるときに呼ばれる */
        @Override
        public View getView(int pos, View convertview, ViewGroup parent){

            ViewHolder holder;

            /* 再利用されるビューが無い場合、 */
            if( convertview == null ){
                convertview = layoutInflater.inflate(
                        R.layout.list_item,
                        parent,
                        false
                );
                holder = new ViewHolder();
                holder.icon = (ImageView) convertview.findViewById(R.id.questionicon);
                holder.info = (TextView) convertview.findViewById(R.id.questionInfo);
                holder.summary = (TextView) convertview.findViewById(R.id.questionsummary);
                convertview.setTag(holder);//データ保持
            }else{
                holder = (ViewHolder)convertview.getTag();
            }

            /* Userデータを取ってきて表示 */
            User user = (User)getItem(pos);

            holder.icon.setImageBitmap(user.getIcon());
            holder.info.setText(user.getQ_info());
            holder.summary.setText(user.getQ_summary());

            return convertview;

        }
    }

    //リストに表示するデータ定義
    static class ViewHolder{
        ImageView icon;
        TextView info;
        TextView summary;
    }

    //リストに表示するデータ型クラス
    public class User{
        private Bitmap icon;
        private String q_info;
        private String q_summary;

//        public void setIcon(Bitmap icon) {
//            this.icon = icon;
//        }

        public void setQ_info(String q_info) {
            this.q_info = q_info;
        }

        public void setQ_summary(String q_summary) {
            this.q_summary = q_summary;
        }

        public Bitmap getIcon() {

            return icon;
        }

        public String getQ_info() {
            return q_info;
        }

        public String getQ_summary() {
            return q_summary;
        }

    }
}
