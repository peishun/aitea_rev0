package com.support.qualifications.getqualifications;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class Activity_OptionMenu_TrialExamResult extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_optionmenu_trialexamresult);
        createLineChart();
        createHorizontalBarChart();

        ///////////////////
        // toolbarの表示 //
        ///////////////////

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_optionmenu_trialexamresult);
        toolbar.setTitle("模擬試験結果");
        toolbar.setTitleTextColor(Color.WHITE);
    }

    ////////////////////////
    // 折れ線グラフの設定 //
    ////////////////////////

    private void createLineChart() {
        LineChart lineChart = (LineChart) findViewById(R.id.line_chart);
        //図名を右下に表示
        lineChart.setDescription("成長グラフ");

        lineChart.getAxisRight().setEnabled(false);
        lineChart.getAxisLeft().setEnabled(true);
        lineChart.setDrawGridBackground(true);
        //lineChart.setDrawBarShadow(false);
        lineChart.setEnabled(true);

        lineChart.setTouchEnabled(true);
        lineChart.setPinchZoom(false);
        lineChart.setDoubleTapToZoomEnabled(false);

        //lineChart.setHighlightEnabled(true);
        //lineChart.setDrawHighlightArrow(true);
        //lineChart.setHighlightEnabled(true);

        lineChart.setScaleEnabled(true);

        lineChart.getLegend().setEnabled(true);

        //X軸周り
        XAxis xAxis = lineChart.getXAxis();
        xAxis.setDrawLabels(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(true);
        xAxis.setSpaceBetweenLabels(0);

        lineChart.setData(createLineChartData());

        lineChart.invalidate();
        // アニメーション
        //lineChart.animateY(2000, Easing.EasingOption.EaseInBack);
    }

    ////////////////////////////////
    // 折れ線グラフのデータを設定 //
    ////////////////////////////////
    private LineData createLineChartData() {
        ArrayList<LineDataSet> lineDataSets = new ArrayList<>();

        // X軸
        ArrayList<String> xValues = new ArrayList<>();
        xValues.add("前々回");
        xValues.add("前回");
        xValues.add("今回");

        // value
        ArrayList<Entry> values = new ArrayList<>();
        values.add(new Entry(60, 0));
        values.add(new Entry(70, 1));
        values.add(new Entry(80, 2));

        LineDataSet valuesDataSet = new LineDataSet(values, "正答率");
        valuesDataSet.setColor(ColorTemplate.COLORFUL_COLORS[3]);

        lineDataSets.add(valuesDataSet);

        LineData lineData = new LineData(xValues, lineDataSets);
        return lineData;
    }

    //////////////////////
    // 横棒グラフの設定 //
    //////////////////////
    private void createHorizontalBarChart() {
        HorizontalBarChart horizontalbarChart = (HorizontalBarChart) findViewById(R.id.horizontalbar_chart);
        horizontalbarChart.setDescription("正答率(ジャンル)");

        horizontalbarChart.getAxisRight().setEnabled(false);
        horizontalbarChart.getAxisLeft().setEnabled(true);
        horizontalbarChart.setDrawGridBackground(true);
        horizontalbarChart.setDrawBarShadow(false);
        horizontalbarChart.setEnabled(true);

        horizontalbarChart.setTouchEnabled(true);
        horizontalbarChart.setPinchZoom(false);
        horizontalbarChart.setDoubleTapToZoomEnabled(false);

        //horizontalbarChart.setHighlightEnabled(true);
        horizontalbarChart.setDrawHighlightArrow(true);
        //horizontalbarChart.setHighlightEnabled(true);

        horizontalbarChart.setScaleEnabled(true);

        horizontalbarChart.getLegend().setEnabled(true);

        //X軸周り
        XAxis xAxis = horizontalbarChart.getXAxis();
        xAxis.setDrawLabels(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(true);
        xAxis.setSpaceBetweenLabels(0);

        horizontalbarChart.setData(createHorizontalBarChartData());

        horizontalbarChart.invalidate();
        // アニメーション
        horizontalbarChart.animateY(2000, Easing.EasingOption.EaseInBack);
    }

    //////////////////////////////
    // 横棒グラフのデータを設定 //
    //////////////////////////////

    private BarData createHorizontalBarChartData() {
        ArrayList<BarDataSet> barDataSets = new ArrayList<>();

        // X軸
        ArrayList<String> xValues = new ArrayList<>();
        xValues.add("基礎理論");
        xValues.add("コンピュータシステム");
        xValues.add("技術要素");
        xValues.add("開発技術");
        xValues.add("プロジェクトマネジメント");
        xValues.add("サービスマネジメント");
        xValues.add("システム戦略");
        xValues.add("経営戦略");
        xValues.add("企業と法務");

        // 今回
        ArrayList<BarEntry> valuesA = new ArrayList<>();
        valuesA.add(new BarEntry(100, 0));
        valuesA.add(new BarEntry(200, 1));
        valuesA.add(new BarEntry(300, 2));
        valuesA.add(new BarEntry(400, 3));
        valuesA.add(new BarEntry(500, 4));
        valuesA.add(new BarEntry(600, 5));
        valuesA.add(new BarEntry(400, 6));
        valuesA.add(new BarEntry(500, 7));
        valuesA.add(new BarEntry(600, 8));

        BarDataSet valuesADataSet = new BarDataSet(valuesA, "今回");
        valuesADataSet.setColor(ColorTemplate.COLORFUL_COLORS[0]);

        barDataSets.add(valuesADataSet);

        // 前回
        ArrayList<BarEntry> valuesB = new ArrayList<>();
        valuesB.add(new BarEntry(200, 0));
        valuesB.add(new BarEntry(300, 1));
        valuesB.add(new BarEntry(400, 2));
        valuesB.add(new BarEntry(500, 3));
        valuesB.add(new BarEntry(600, 4));
        valuesB.add(new BarEntry(700, 5));
        valuesB.add(new BarEntry(500, 6));
        valuesB.add(new BarEntry(600, 7));
        valuesB.add(new BarEntry(700, 8));

        BarDataSet valuesBDataSet = new BarDataSet(valuesB, "前回");
        valuesBDataSet.setColor(ColorTemplate.COLORFUL_COLORS[1]);

        barDataSets.add(valuesBDataSet);

        // 前々回
        ArrayList<BarEntry> valuesC = new ArrayList<>();
        valuesC.add(new BarEntry(200, 0));
        valuesC.add(new BarEntry(300, 1));
        valuesC.add(new BarEntry(400, 2));
        valuesC.add(new BarEntry(500, 3));
        valuesC.add(new BarEntry(600, 4));
        valuesC.add(new BarEntry(700, 5));
        valuesC.add(new BarEntry(500, 6));
        valuesC.add(new BarEntry(600, 7));
        valuesC.add(new BarEntry(700, 8));

        BarDataSet valuesCDataSet = new BarDataSet(valuesC, "前々回");
        valuesCDataSet.setColor(ColorTemplate.COLORFUL_COLORS[2]);

        barDataSets.add(valuesCDataSet);

        BarData horizontalbarData = new BarData(xValues, barDataSets);
        return horizontalbarData;
    }
}