package com.support.qualifications.getqualifications;

import java.io.Serializable;

/**
 * tbl_answer_archiveのデータ型クラス
 */
public class answer_archive_data implements Serializable{

	private String user_answer_id;
	private String q_id;
	private String user_answer;
	private Boolean correct_mistake;
	private String user_answer_date;

}
