package com.support.qualifications.getqualifications;

/**
 * Created by joho on 2016/01/12.
 */
public class Finishflag {

    private boolean flag;

    public boolean getFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
}
