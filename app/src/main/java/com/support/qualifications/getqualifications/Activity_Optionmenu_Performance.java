package com.support.qualifications.getqualifications;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;

import java.util.ArrayList;

public class Activity_OptionMenu_Performance extends Activity {

    final int CORRECT_RATE_COUNT = 5;
    final String DESC = "DESC";
    final String ASC = "ASC";
    private genre_info_data[] s_class_correct_rate_asc;
    private genre_info_data[] s_class_correct_rate_desc;
    private genre_info_data[] m_class_correct_rate_asc;
    private genre_info_data[] m_class_correct_rate_desc;
    private genre_info_data[] l_class_correct_rate_asc;
    private genre_info_data[] l_class_correct_rate_desc;
    private genre_info_data[] field_correct_rate;
    private TableLayout tableLayout_small;
    private TableLayout tableLayout_middle;
    private TableLayout tableLayout_large;
    private float field_correct_rate0;
    private float field_correct_rate1;
    private float field_correct_rate2;
    private String field_name0 = null;
    private String field_name1 = null;
    private String field_name2 = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_optionmenu_performance);

        s_class_correct_rate_asc = get_s_class_correct_rate(CORRECT_RATE_COUNT, ASC);
        s_class_correct_rate_desc = get_s_class_correct_rate(CORRECT_RATE_COUNT, DESC);
        m_class_correct_rate_asc = get_m_class_correct_rate(CORRECT_RATE_COUNT, ASC);
        m_class_correct_rate_desc = get_m_class_correct_rate(CORRECT_RATE_COUNT, DESC);
        l_class_correct_rate_asc = get_l_class_correct_rate(CORRECT_RATE_COUNT, ASC);
        l_class_correct_rate_desc = get_l_class_correct_rate(CORRECT_RATE_COUNT, DESC);
        field_correct_rate = get_field_correct_rate();

        /** toolbarの表示 **/
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_optionmenu_performance);
        toolbar.setTitle("通算成績");
        toolbar.setTitleTextColor(Color.WHITE);

        /** RadioButtonタッチ時の処理 **/
        RadioGroup radioGroup_large = (RadioGroup) findViewById(R.id.radiogroup_large);
        RadioGroup radioGroup_middle = (RadioGroup) findViewById(R.id.radiogroup_middle);
        RadioGroup radioGroup_small = (RadioGroup) findViewById(R.id.radiogroup_small);
        radioGroup_large.check(R.id.radiobutton_bad_large);
        radioGroup_middle.check(R.id.radiobutton_bad_middle);
        radioGroup_small.check(R.id.radiobutton_bad_small);
        radioGroup_large.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = (RadioButton) findViewById(checkedId);
                boolean checked = radioButton.isChecked();
                switch (radioButton.getId()) {
                    case R.id.radiobutton_bad_large:
                        if (checked) {
                            tableLayout_large.removeAllViews();
                            class_correct_rate(tableLayout_large, l_class_correct_rate_asc);
                        }
                        break;
                    case R.id.radiobutton_good_large:
                        if (checked) {
                            tableLayout_large.removeAllViews();
                            class_correct_rate(tableLayout_large, l_class_correct_rate_desc);
                        }
                        break;
                    default:
                        break;
                }
            }
        });
        radioGroup_middle.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = (RadioButton) findViewById(checkedId);
                boolean checked = radioButton.isChecked();
                switch (radioButton.getId()) {
                    case R.id.radiobutton_bad_middle:
                        if (checked) {
                            tableLayout_middle.removeAllViews();
                            class_correct_rate(tableLayout_middle, m_class_correct_rate_asc);
                        }
                        break;
                    case R.id.radiobutton_good_middle:
                        if (checked) {
                            tableLayout_middle.removeAllViews();
                            class_correct_rate(tableLayout_middle, m_class_correct_rate_desc);
                        }
                        break;
                    default:
                        break;
                }
            }
        });
        radioGroup_small.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = (RadioButton) findViewById(checkedId);
                boolean checked = radioButton.isChecked();
                switch (radioButton.getId()) {
                    case R.id.radiobutton_bad_small:
                        if (checked) {
                            tableLayout_small.removeAllViews();
                            class_correct_rate(tableLayout_small, s_class_correct_rate_asc);
                        }
                        break;
                    case R.id.radiobutton_good_small:
                        if (checked) {
                            tableLayout_small.removeAllViews();
                            class_correct_rate(tableLayout_small, s_class_correct_rate_desc);
                        }
                        break;
                    default:
                        break;
                }
            }
        });

        /** TableLayoutにDBから取得したデータを設定 **/
        tableLayout_large = (TableLayout)findViewById(R.id.tableLayout_large);
        class_correct_rate(tableLayout_large, l_class_correct_rate_asc);
        tableLayout_middle = (TableLayout)findViewById(R.id.tableLayout_middle);
        class_correct_rate(tableLayout_middle, m_class_correct_rate_asc);
        tableLayout_small = (TableLayout)findViewById(R.id.tableLayout_small);
        class_correct_rate(tableLayout_small, s_class_correct_rate_asc);

//        RadarChartの目盛が動的に調整されてしまうため、解決するまでコメントアウト
//        20150115 Kazuki Nakano
//
//        /** RadarCharの準備 **/
//        RadarChart chart = (RadarChart) findViewById(R.id.chart);
//        RadarData data = new RadarData(getXAxisValues(), getRadarDataSet());
//        chart.setData(data);
//        chart.setDescription("直近の回答結果を分野別に纏めています。");
//        chart.animateXY(100, 100);
//        chart.invalidate();
//        chart.setRotationEnabled(false); // 回転可能かどうか
//    }
//
//    private ArrayList<RadarDataSet> getRadarDataSet() {
//        ArrayList<RadarDataSet> dataSets = null;
//
//        try {
//            field_correct_rate0 = (float) field_correct_rate[0].getField_correct_rate();
//        }catch(IndexOutOfBoundsException e){
//            // field_correct_rate not exist
//            field_correct_rate0 = 0.0f;
//        }
//
//        try {
//            field_correct_rate1 = (float) field_correct_rate[1].getField_correct_rate();
//        }catch(IndexOutOfBoundsException e){
//            // field_correct_rate not exist
//            field_correct_rate1 = 0.0f;
//        }
//
//        try {
//            field_correct_rate2 = (float) field_correct_rate[2].getField_correct_rate();
//        }catch(IndexOutOfBoundsException e){
//            // field_correct_rate not exist
//            field_correct_rate2 = 0.0f;
//        }
//
//        // -----------------------------------2016/01/14 西宮追記　そのばしのぎコードSTART
//        String[] fields = {
//                "テクノロジ",
//                "マネジメント",
//                "ストラテジ"
//        };
//
//        try {
//            field_name0 = field_correct_rate[0].getField_name();
//        }catch(IndexOutOfBoundsException e){
//            // 分野が1つも取得できなかったとき
//            field_name0 = fields[0];
//            field_name1 = fields[1];
//            field_name2 = fields[2];
//        }
//
//        try {
//            field_name1 = field_correct_rate[1].getField_name();
//            try {
//                field_name2 = field_correct_rate[2].getField_name();
//            }catch(IndexOutOfBoundsException e){
//                // 分野が1つ取得できなかったとき
//                int i, j, k;
//                // 1番目の分野は何だったか検索
//                for(i=0; i<3; i++) if(field_name0.equals(fields[i])) break;
//                // 2番目の分野は何だったか検索
//                for(j=0; j<3; j++) if(field_name1.equals(fields[j])) break;
//                // 検索でヒットした分野以外を3番目に設定
//                for(k=0; k<3; k++){
//                    if(k != i && k != j){
//                        field_name2 = fields[k];
//                    }
//                }
//            }
//        }catch(IndexOutOfBoundsException e){
//            // 分野が1つ取得できたとき
//            int i=0;
//            // 1番目の分野は何だったか検索
//            for(i=0; i<3; i++) if(field_name0.equals(fields[i])) break;
//            // 検索でヒットした分野以外を2番目と3番目に設定
//            for(int j=0; j<3; j++){
//                if(i != j && field_name1 == null){
//                    field_name1 = fields[j];
//                } else if(i != j && field_name2 == null){
//                    field_name2 = fields[j];
//                }
//            }
//        }
//        // -----------------------------------2016/01/14 西宮追記　そのばしのぎコードEND
//
//        ArrayList<Entry> valueSet1 = new ArrayList<>();
//        Entry v1e1 = new Entry(field_correct_rate0, 0); // テクノロジ
//        valueSet1.add(v1e1);
//        Entry v1e2 = new Entry(field_correct_rate1, 1); // ストラテジ
//        valueSet1.add(v1e2);
//        Entry v1e3 = new Entry(field_correct_rate2, 2); // マネジメント
//        valueSet1.add(v1e3);
//
//        RadarDataSet radarDataSet1 = new RadarDataSet(valueSet1, "通算成績");
//        radarDataSet1.setColor(Color.rgb(255, 99, 71));
//
//        dataSets = new ArrayList<>();
//        dataSets.add(radarDataSet1);
//        return dataSets;
//    }
//
//    private ArrayList<String> getXAxisValues() {
//        ArrayList<String> xAxis = new ArrayList<>();
//
//        xAxis.add("テクノロジ");
//        xAxis.add("マネジメント");
//        xAxis.add("ストラテジ");
//
//        return xAxis;
    }

    private genre_info_data[] get_s_class_correct_rate(int limit, String order){

        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        /* Cursor(sql文実行結果を取得するもの)を生成し、sql文実行 */
        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_s_class_correct_rate(limit, order), null);

        /* 行数文の大きさのデータクラス型配列 */
        genre_info_data[] out_g_i_data = new genre_info_data[c.getCount()];

        int i = 0;
        /* sqlで実行した結果取得したテーブルを1行ずつ参照 */
        while (c.moveToNext()){
            out_g_i_data[i] = new genre_info_data();
            out_g_i_data[i].setField_code(c.getInt(c.getColumnIndex(AiteaContract.GENRE_INFO.FIELD_CODE)));
            out_g_i_data[i].setField_name(c.getString(c.getColumnIndex(AiteaContract.GENRE_INFO.FIELD_NAME)));
            out_g_i_data[i].setS_class_code(c.getInt(c.getColumnIndex(AiteaContract.GENRE_INFO.S_CLASS_CODE)));
            out_g_i_data[i].setS_class_name(c.getString(c.getColumnIndex(AiteaContract.GENRE_INFO.S_CLASS_NAME)));
            out_g_i_data[i].setS_class_correct_rate(c.getDouble(c.getColumnIndex(AiteaContract.GENRE_INFO.S_CLASS_CORRECT_RATE)));
            i++;
        }

        c.close();
        db.close();

        return out_g_i_data;
    }

    private genre_info_data[] get_m_class_correct_rate(int limit, String order){

        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        /* Cursor(sql文実行結果を取得するもの)を生成し、sql文実行 */
        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_m_class_correct_rate(limit, order), null);

        /* 行数文の大きさのデータクラス型配列 */
        genre_info_data[] out_g_i_data = new genre_info_data[c.getCount()];

        int i = 0;
        /* sqlで実行した結果取得したテーブルを1行ずつ参照 */
        while (c.moveToNext()){
            out_g_i_data[i] = new genre_info_data();
            out_g_i_data[i].setField_code(c.getInt(c.getColumnIndex(AiteaContract.GENRE_INFO.FIELD_CODE)));
            out_g_i_data[i].setField_name(c.getString(c.getColumnIndex(AiteaContract.GENRE_INFO.FIELD_NAME)));
            out_g_i_data[i].setM_class_code(c.getInt(c.getColumnIndex(AiteaContract.GENRE_INFO.M_CLASS_CODE)));
            out_g_i_data[i].setM_class_name(c.getString(c.getColumnIndex(AiteaContract.GENRE_INFO.M_CLASS_NAME)));
            out_g_i_data[i].setM_class_correct_rate(c.getDouble(c.getColumnIndex(AiteaContract.GENRE_INFO.M_CLASS_CORRECT_RATE)));
            i++;
        }

        c.close();
        db.close();

        return out_g_i_data;
    }

    private genre_info_data[] get_l_class_correct_rate(int limit, String order){

        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        /* Cursor(sql文実行結果を取得するもの)を生成し、sql文実行 */
        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_l_class_correct_rate(limit, order), null);

        /* 行数文の大きさのデータクラス型配列 */
        genre_info_data[] out_g_i_data = new genre_info_data[c.getCount()];

        int i = 0;
        /* sqlで実行した結果取得したテーブルを1行ずつ参照 */
        while (c.moveToNext()){
            out_g_i_data[i] = new genre_info_data();
            out_g_i_data[i].setField_code(c.getInt(c.getColumnIndex(AiteaContract.GENRE_INFO.FIELD_CODE)));
            out_g_i_data[i].setField_name(c.getString(c.getColumnIndex(AiteaContract.GENRE_INFO.FIELD_NAME)));
            out_g_i_data[i].setL_class_code(c.getInt(c.getColumnIndex(AiteaContract.GENRE_INFO.L_CLASS_CODE)));
            out_g_i_data[i].setL_class_name(c.getString(c.getColumnIndex(AiteaContract.GENRE_INFO.L_CLASS_NAME)));
            out_g_i_data[i].setL_class_correct_rate(c.getDouble(c.getColumnIndex(AiteaContract.GENRE_INFO.L_CLASS_CORRECT_RATE)));
            i++;
        }

        c.close();
        db.close();

        return out_g_i_data;
    }

    private genre_info_data[] get_field_correct_rate(){

        /* DatabaseとOpenhelper(Databaseを操作するもの)を生成 */
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        /* Cursor(sql文実行結果を取得するもの)を生成し、sql文実行 */
        Cursor c = null;
        c = db.rawQuery(DB_makesql.get_field_correct_rate(), null);

        /* 行数文の大きさのデータクラス型配列 */
        genre_info_data[] out_g_i_data = new genre_info_data[c.getCount()];

        int i = 0;
        /* sqlで実行した結果取得したテーブルを1行ずつ参照 */
        while (c.moveToNext()){
            out_g_i_data[i] = new genre_info_data();
            out_g_i_data[i].setField_code(c.getInt(c.getColumnIndex(AiteaContract.GENRE_INFO.FIELD_CODE)));
            out_g_i_data[i].setField_name(c.getString(c.getColumnIndex(AiteaContract.GENRE_INFO.FIELD_NAME)));
            out_g_i_data[i].setField_correct_rate(c.getDouble(c.getColumnIndex(AiteaContract.GENRE_INFO.FIELD_CORRECT_RATE)));
            i++;
        }

        c.close();
        db.close();

        return out_g_i_data;
    }

    /**
     * TableLayoutにTableRowをaddする(昇順、降順はSQL文で指定するため、ここで意識する必要はない)
     * @param tableLayout 大、中、小分類のTableLayoutのid
     * @param class_correct_rate DBから取得した大、中、小分類の正答率
     */
    private void class_correct_rate(TableLayout tableLayout ,genre_info_data[] class_correct_rate){
        for(int i = 0; i < class_correct_rate.length; i++) {
            TableRow tableRow = (TableRow) getLayoutInflater().inflate(R.layout.layout_tablerow_optionmenu_performnace, null);
            TextView name = (TextView)tableRow.findViewById(R.id.ranking);
            int rank = i + 1;
            name.setText(rank + "位");
            TextView field = (TextView)tableRow.findViewById(R.id.field);
            field.setText(class_correct_rate[i].getField_name());

            if(class_correct_rate[i].getS_class_code() > 0){
                TextView small_class_item = (TextView)tableRow.findViewById(R.id.class_item);
                small_class_item.setText(class_correct_rate[i].getS_class_name());
                TextView small_correct_late = (TextView)tableRow.findViewById(R.id.correct_rate);
                small_correct_late.setText(class_correct_rate[i].getS_class_correct_rate() * 100 + "%");
            }else if(class_correct_rate[i].getM_class_code() > 0){
                TextView middle_class_item = (TextView)tableRow.findViewById(R.id.class_item);
                middle_class_item.setText(class_correct_rate[i].getM_class_name());
                TextView middle_correct_late = (TextView)tableRow.findViewById(R.id.correct_rate);
                middle_correct_late.setText(class_correct_rate[i].getM_class_correct_rate() * 100 + "%");
            }else{
                TextView large_class_item = (TextView)tableRow.findViewById(R.id.class_item);
                large_class_item.setText(class_correct_rate[i].getL_class_name());
                TextView large_correct_late = (TextView)tableRow.findViewById(R.id.correct_rate);
                large_correct_late.setText(class_correct_rate[i].getL_class_correct_rate() * 100 + "%");
            }
            tableLayout.addView(tableRow, new TableLayout.LayoutParams());
        }
    }
}
