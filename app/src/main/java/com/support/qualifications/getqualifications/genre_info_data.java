package com.support.qualifications.getqualifications;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.BaseColumns;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;

/**
 * tbl_genre_infoのデータ型クラス
 */
public class genre_info_data implements Serializable {

    private int field_code;
    private String field_name;
    private int l_class_code;
    private String l_class_name;
    private int m_class_code;
    private String m_class_name;
    private int s_class_code;
    private String s_class_name;
    private double s_class_denominator;
    private double s_class_molecule;
    private double s_class_correct_rate;
    private double m_class_correct_rate;
    private double l_class_correct_rate;
    private double field_correct_rate;


    public int getField_code() {
        return field_code;
    }

    public void setField_code(int field_code) {
        this.field_code = field_code;
    }

    public String getField_name() {
        return field_name;
    }

    public void setField_name(String field_name) {
        this.field_name = field_name;
    }

    public int getL_class_code() {
        return l_class_code;
    }

    public void setL_class_code(int l_class_code) {
        this.l_class_code = l_class_code;
    }

    public String getL_class_name() {
        return l_class_name;
    }

    public void setL_class_name(String l_class_name) {
        this.l_class_name = l_class_name;
    }

    public int getM_class_code() {
        return m_class_code;
    }

    public void setM_class_code(int m_class_code) {
        this.m_class_code = m_class_code;
    }

    public String getM_class_name() {
        return m_class_name;
    }

    public void setM_class_name(String m_class_name) {
        this.m_class_name = m_class_name;
    }

    public int getS_class_code() {
        return s_class_code;
    }

    public void setS_class_code(int s_class_code) {
        this.s_class_code = s_class_code;
    }

    public String getS_class_name() {
        return s_class_name;
    }

    public void setS_class_name(String s_class_name) {
        this.s_class_name = s_class_name;
    }

    public double getS_class_denominator() {
        return s_class_denominator;
    }

    public void setS_class_denominator(double s_class_denominator) {
        this.s_class_denominator = s_class_denominator;
    }

    public double getS_class_molecule() {
        return s_class_molecule;
    }

    public void setS_class_molecule(double s_class_molecule) {
        this.s_class_molecule = s_class_molecule;
    }

    public double getS_class_correct_rate() {
        return s_class_correct_rate;
    }

    public void setS_class_correct_rate(double s_class_correct_rate) {
        this.s_class_correct_rate = s_class_correct_rate;
    }

    public double getM_class_correct_rate() {
        return m_class_correct_rate;
    }

    public void setM_class_correct_rate(double m_class_correct_rate) {
        this.m_class_correct_rate = m_class_correct_rate;
    }

    public double getL_class_correct_rate() {
        return l_class_correct_rate;
    }

    public void setL_class_correct_rate(double l_class_correct_rate) {
        this.l_class_correct_rate = l_class_correct_rate;
    }

    public double getField_correct_rate() {
        return field_correct_rate;
    }

    public void setField_correct_rate(double field_correct_rate) {
        this.field_correct_rate = field_correct_rate;
    }




}