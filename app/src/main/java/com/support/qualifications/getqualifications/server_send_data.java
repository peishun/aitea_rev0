package com.support.qualifications.getqualifications;

import java.io.Serializable;

/**
 * サーバ送信用のデータクラス
 */
public class server_send_data implements Serializable {

    private String exam_category;
    private String question_id;
    private String user_answer;
    private String correct_mistake;

    public String getExam_category() {
        return exam_category;
    }

    public void setExam_category(String exam_category) {
        this.exam_category = exam_category;
    }

    public String getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(String question_id) {
        this.question_id = question_id;
    }

    public String getUser_answer() {
        return user_answer;
    }

    public void setUser_answer(String user_answer) {
        this.user_answer = user_answer;
    }

    public String getCorrect_mistake() {
        return correct_mistake;
    }

    public void setCorrect_mistake(String correct_mistake) {
        this.correct_mistake = correct_mistake;
    }
}
