package com.support.qualifications.getqualifications;

import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by joho on 2015/12/25.
 */
public final class set_position_WebViewClient extends WebViewClient {

    @Override
    public void onPageFinished(final WebView view , String url) {
        super.onPageFinished(view, url);
        //ロード完了時にやりたい事を書く

        view.postDelayed(new Runnable() {
            @Override
            public void run() {
                view.setScrollX(600);
                view.setScrollY(50);
            }
        }, 100);

    }
}