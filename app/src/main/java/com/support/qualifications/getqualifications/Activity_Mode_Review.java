// 参考URL http://ichitcltk.hustle.ne.jp/gudon2/index.php?pageType=file&id=Android028_ListView1

package com.support.qualifications.getqualifications;

import android.app.ListActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ArrayAdapter;

public class Activity_Mode_Review extends ListActivity {
    private String[] mStrings = {
            "平成27年 問1 10進数の演算式7/32の結果…",
            "平成27年 問1 10進数の演算式7/32の結果…",
            "平成27年 問1 10進数の演算式7/32の結果…",
            "平成27年 問1 10進数の演算式7/32の結果…",
            "平成27年 問1 10進数の演算式7/32の結果…",
            "平成27年 問1 10進数の演算式7/32の結果…",
            "平成27年 問1 10進数の演算式7/32の結果…",
            "平成27年 問1 10進数の演算式7/32の結果…",
            "平成27年 問1 10進数の演算式7/32の結果…",
            "平成27年 問1 10進数の演算式7/32の結果…",
            "平成27年 問1 10進数の演算式7/32の結果…",
            "平成27年 問1 10進数の演算式7/32の結果…",
            "平成27年 問1 10進数の演算式7/32の結果…",
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, mStrings);
        setListAdapter(adapter);

        ///////////////////
        // toolbarの表示 //
        ///////////////////

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_mode_review);
//        toolbar.setTitle("復習");
    }
}
